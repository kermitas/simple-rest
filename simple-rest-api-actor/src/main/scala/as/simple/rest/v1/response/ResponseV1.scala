package as.simple.rest.v1.response

import as.simple.rest.v1.request.{ RequestV1, InvitationRequestV1, AddInviteeRequestV1, GetInvitationsRequestV1 }
import as.simple.rest.simplerest.Invitee
import scala.util.Try

sealed trait ResponseV1 extends Serializable {
  val request: Option[RequestV1]
  val processingTime: Option[Long]
}

sealed trait InvitationResponseV1 extends ResponseV1 {
  val request: Option[InvitationRequestV1]
}

case class AddInviteeResponseV1(
  addingResult: Try[Boolean],

  request: Option[AddInviteeRequestV1],
  processingTime: Option[Long]
) extends InvitationResponseV1

case class GetInvitationsResponseV1(
  invitations: Try[Seq[Invitee]],

  request: Option[GetInvitationsRequestV1],
  processingTime: Option[Long]
) extends InvitationResponseV1
