package as.simple.rest.v1.actor

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import as.simple.rest.v1.RequestResponseV1
import as.simple.rest.v1.request.RequestV1
import as.simple.rest.v1.response.ResponseV1
import scala.concurrent.{ ExecutionContext, Future }

/**
 * The implementation of [[as.simple.rest.v1.RequestResponseV1]] that will use actor under the hood.
 *
 * @param requestResponseV1Actor the actor that implements [[as.simple.rest.v1.actor.RequestResponseV1ActorApi]]
 * @param responseTimeout timeout while waiting for response
 */
class ActorBasedRequestResponseV1(requestResponseV1Actor: ActorRef, responseTimeout: Timeout)(implicit ec: ExecutionContext) extends RequestResponseV1 {

  override def apply(request: RequestV1): Future[ResponseV1] = requestResponseV1Actor.ask(request)(responseTimeout).mapTo[ResponseV1]

}