package as.simple.rest.v1

import as.simple.rest.v1.request.RequestV1
import as.simple.rest.v1.response.ResponseV1
import scala.concurrent.Future

/**
 * Main interface that perform this project's business: on incoming request it creates response.
 */
trait RequestResponseV1 {

  /**
   * request -> to -> response transformer
   */
  def apply(request: RequestV1): Future[ResponseV1]
}
