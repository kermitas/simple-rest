package as.simple.rest.v1.request

import as.simple.rest.simplerest.Invitee

object RequestV1 {
  val restVersion = "v1"
}

sealed trait RequestV1 extends Serializable {
  val timeoutInMs: Option[Long]
  val processingStartTime: Long = System.currentTimeMillis
}

sealed trait InvitationRequestV1 extends RequestV1

object InvitationRequestV1 {
  val restResource = "invitation"
}

case class AddInviteeRequestV1(
  invitee: Invitee,

  timeoutInMs: Option[Long]
) extends InvitationRequestV1

case class GetInvitationsRequestV1(
  timeoutInMs: Option[Long]
) extends InvitationRequestV1