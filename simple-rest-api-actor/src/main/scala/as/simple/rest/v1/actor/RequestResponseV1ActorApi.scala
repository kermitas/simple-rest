package as.simple.rest.v1.actor

import as.simple.rest.v1.request.RequestV1
import as.simple.rest.v1.response.ResponseV1

/**
 * Actor's (message) API.
 *
 * When communicating with actor that implements this API just send [[RequestV1]] to it and get [[ResponseV1]] as a response (or nothing so remember to set timeout).
 */
object RequestResponseV1ActorApi {
  sealed trait Message extends Serializable
  sealed trait OutgoingMessage extends Message

  /**
   * Send to parent actor when this actor is fully initialized and ready to work. Remember that actor can be restarted by supervisor and you can
   * receive this message more than once.
   */
  case object InitializationComplete extends OutgoingMessage

}
