package as.simple.rest

object Main {
  /**
   * The main configuration key under which full configuration will be read.
   */
  val configurationTopKeyName = "simpleRest"

  /**
   * JVM main entry point.
   */
  def main(args: Array[String]): Unit = (new Main)(args)

  /**
   * Examine startup requirements.
   */
  protected def examineStartupRequirements: Unit = {
    {
      import java.util.Locale
      val expectedLocale = Locale.US
      require(Locale.getDefault.equals(expectedLocale), s""""$expectedLocale" locale is required (but current is "${Locale.getDefault}")""")
    }
    {
      import java.nio.charset.Charset
      val expectedCharset = Charset.forName("UTF-8")
      require(Charset.defaultCharset.equals(expectedCharset), s""""$expectedCharset" charset encoding is required (but current is "${Charset.defaultCharset}")""")
    }
  }

  /**
   * Read configuration in a default manner (it search for "application.XYZ" file, https://github.com/typesafehub/config).
   */
  protected def readConfiguration: MainConfig = {
    import com.typesafe.config.ConfigFactory
    import net.ceedubs.ficus.Ficus._
    import net.ceedubs.ficus.readers.ArbitraryTypeReader._
    import net.ceedubs.ficus.readers.InetAddressReader._

    ConfigFactory.load.as[MainConfig](Main.configurationTopKeyName)
  }
}

class Main {

  import Main._

  def apply(args: Array[String]): Unit = {
    import akka.actor.{ ActorSystem, Props }
    import as.simple.rest.actor.main.{ Main => MainActor }
    import scala.util.Try

    println(s"Hello from ${getClass.getName}!")

    val mainConfig = readConfiguration

    println(s"My configuration (${classOf[MainConfig].getName}) is: $mainConfig.")

    // create actor system
    val actorSystem = ActorSystem(
      name = mainConfig.actorSystemName
    )

    examineStartupRequirements

    try {
      // create main actor
      actorSystem.actorOf(
        props = Props(new MainActor(mainConfig.main)),
        name = classOf[MainActor].getSimpleName
      )

    } catch {
      case e: Exception => {
        Try { actorSystem.shutdown }
        throw e
      }
    }
  }
}
