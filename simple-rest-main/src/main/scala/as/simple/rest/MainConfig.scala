package as.simple.rest

import as.simple.rest.actor.main.{ MainConfig => MainActorConfig }

/**
 * @param actorSystemName the name of actor system
 */
case class MainConfig(
  actorSystemName: String,
  main: MainActorConfig
)