package as.simple.rest.actor.resources

import akka.actor.{ OneForOneStrategy, Cancellable, ActorRef, SupervisorStrategy, Terminated }
import as.simple.rest.actor.http.AkkaHttpManager
import com.timgroup.statsd.StatsDClient
import as.simple.rest.v1.actor.RequestResponseV1ActorApi

object ResourcesManager {
  sealed trait State
  case object Uninitialized extends State
  case object Initializing extends State
  case object Operating extends State

  sealed trait StateData
  case class UninitializedStateData(resourcesInitializationTimeout: Cancellable) extends StateData
  case class InitializingStateData(resourcesInitializationTimeout: Cancellable, statisticalActor: Option[ActorRef], requestResponseV1Actor: ActorRef, akkaHttpManager: Option[ActorRef] = None) extends StateData
  case class OperatingStateData(akkaHttpManager: ActorRef, statisticalActor: Option[ActorRef], requestResponseV1Actor: ActorRef) extends StateData

  sealed trait Message extends Serializable
  sealed trait InternalMessage extends Message
  object ResourcesInitializationTimeout extends InternalMessage
  sealed trait OutgoingMessage extends Message

  /**
   * Send to parent actor when this actor is fully initialized and ready to work. Remember that actor can be restarted by supervisor and you can
   * receive this message more than once.
   */
  case object InitializationComplete extends OutgoingMessage
}

/**
 * This actor is responsible for creating resources and pass it to child actor.
 */
class ResourcesManager(config: ResourcesManagerConfig, statsd: StatsDClient) extends ResourcesManagerImpl(config, statsd) {

  import ResourcesManager._

  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = config.maxNrOfRetries, withinTimeRange = config.withinTimeRange) {
      case e: Exception => SupervisorStrategy.Restart
    }

  {
    val resourcesInitializationTimeout = context.system.scheduler.scheduleOnce(config.initializationTimeout, self, ResourcesInitializationTimeout)
    startWith(Uninitialized, new UninitializedStateData(resourcesInitializationTimeout))
  }

  when(Uninitialized) {
    case Event(Uninitialized, sd: UninitializedStateData) => init(sd)
  }

  when(Initializing) {
    case Event(RequestResponseV1ActorApi.InitializationComplete, sd: InitializingStateData) => requestResponseV1ActorInitializationComplete(sd)
    case Event(AkkaHttpManager.InitializationComplete, sd: InitializingStateData) => akkaHttpManagerInitializationComplete(sd)
    case Event(ResourcesInitializationTimeout, _) => throw new Exception(s"Resources initialization timeout (${config.initializationTimeout}).")
  }

  when(Operating) {
    case Event(true, _) => stay // nothing to do in this state
  }

  onTransition {
    case fromState -> toState => log.debug(s"changing state from ${fromState.getClass.getSimpleName} to ${toState.getClass.getSimpleName}")
  }

  whenUnhandled {
    case Event(Terminated(deadActor), _) => throw new Exception(s"""Watched actor "$deadActor" is dead, cannot continue.""")

    case Event(initializationComplete @ RequestResponseV1ActorApi.InitializationComplete, _) => {
      // needed when child actor is restarted, for example: because of its exception (problem)
      log.warning(s"skipping ${initializationComplete.getClass.getName} message from $sender in state $stateName")
      stay
    }

    case Event(initializationComplete @ AkkaHttpManager.InitializationComplete, _) => {
      // needed when child actor is restarted, for example: because of its exception (problem)
      log.warning(s"skipping ${initializationComplete.getClass.getName} message from $sender in state $stateName")
      stay
    }

    case Event(unhandledMessage, currentStateData) =>
      throw new Exception(s"Received unhandled message ${unhandledMessage.getClass.getName} ($unhandledMessage) from $sender in state $stateName (state data $currentStateData).")
  }

  onTermination {
    case StopEvent(reason, currentState, stateData) => terminate(reason, currentState, stateData)
  }

  initialize
  self ! Uninitialized
}
