package as.simple.rest.actor.resources.stats

import akka.actor.{ Actor, ActorLogging }

import scala.collection.mutable
import scala.concurrent.duration._
import scala.language.postfixOps

object StatisticalActor {
  val defaultPrintStatsInterval = 10 seconds
}

/**
 * This is very simple statistics collector (and printing) actor.
 *
 * Just send to it string or (string, value), it will sum all values and print from time to time. It works similarly as statsd "increment" method.
 *
 * Example of output after sending "ok" (or ("ok,1)) to this actor:
 *    ------------------------------> ok->1.07/s(one 937.50ms);ALL=1.07/s(one 937.50ms) <------------------------------
 *
 * Instead of sending "ok" and "err" to this actor you can send whatever you want. For example "in" and "out".
 * Section "ALL=1.07/s(one 937.50ms)" is a sum of all keys that you send (for example "ok" and "err").
 *
 * @param printStatsInterval time interval of printing statistics to the out (level INFO)
 */
class StatisticalActor(printStatsInterval: FiniteDuration = StatisticalActor.defaultPrintStatsInterval) extends Actor with ActorLogging {

  protected val counters = mutable.Map[String, Long]()
  protected val printStatsIntervalInMillis: Double = if (printStatsInterval.toMillis == 0) 1 else printStatsInterval.toMillis
  protected val printStatsIntervalInSecond: Double = if (printStatsInterval.toSeconds == 0) 1 else printStatsInterval.toSeconds
  protected val printStatisticsTimer = context.system.scheduler.schedule(printStatsInterval, printStatsInterval, self, true)(context.dispatcher)

  override def postStop = {
    printStatisticsTimer.cancel
    super.postStop
  }

  override def receive = {

    case key: String                => counters.put(key, counters.get(key).getOrElse(0L) + 1)

    case (key: String, value: Int)  => counters.put(key, counters.get(key).getOrElse(0L) + value)

    case (key: String, value: Long) => counters.put(key, counters.get(key).getOrElse(0L) + value)

    case true => if (!counters.isEmpty) {
      val sum = counters.values.foldLeft(0L)(_ + _)
      val sumTxt = "ALL=%.2f/s(one %sms)".format(
        sum / printStatsIntervalInSecond,
        if (sum != 0) "%.2f".format(printStatsIntervalInMillis / sum) else "???"
      )

      val s = for (kv <- counters) yield "%s->%.2f/s(one %sms)".format(
        kv._1,
        kv._2 / printStatsIntervalInSecond,
        if (kv._2 != 0) "%.2f".format(printStatsIntervalInMillis / kv._2) else "???"
      )

      log.info(s" ------------------------------> ${s.mkString(",")};$sumTxt <------------------------------")

      counters.keys.toSeq.foreach(counters.put(_, 0L))
    }

    case unhandledMessage => log.warning(s"Received unhandled message ${unhandledMessage.getClass.getName} ($unhandledMessage) from $sender.")
  }
}