package as.simple.rest.actor.resources

import akka.actor.{ FSM, Props, ActorRef, PoisonPill }
import as.simple.rest.actor.resources.stats.{ StatisticalActor, NoopStatisticalActor }
import scala.util.Try
import as.simple.rest.v1.actor.{ RequestResponseV1Actor, RequestResponseV1Router }
import as.simple.rest.actor.http.AkkaHttpManager
import com.timgroup.statsd.StatsDClient

/**
 * Implementation for an actor.
 */
class ResourcesManagerImpl(config: ResourcesManagerConfig, statsd: StatsDClient) extends FSM[ResourcesManager.State, ResourcesManager.StateData] {

  protected implicit val executionContext = context.dispatcher

  /**
   * Start parallel initialization of all needed resources.
   */
  protected def init(sd: ResourcesManager.UninitializedStateData): State = {

    var statisticalActor: Option[ActorRef] = None
    var requestResponseV1Actor: Option[ActorRef] = None

    try {
      statisticalActor = config.printInternallyCollectedStatsTimeInterval match {
        case Some(printInternallyCollectedStatsTimeInterval) => Some(
          context.actorOf(
            props = Props(new StatisticalActor(printInternallyCollectedStatsTimeInterval)),
            name = classOf[StatisticalActor].getSimpleName
          )
        )

        case None => Some(
          context.actorOf(
            props = Props[NoopStatisticalActor],
            name = classOf[NoopStatisticalActor].getSimpleName
          )
        )
      }

      statisticalActor.map(context.watch)

      requestResponseV1Actor = {

        val requestResponseV1ActorProps = Props(new RequestResponseV1Actor(config.requestResponseV1Actor, statisticalActor.get, statsd))

        val requestResponseV1Actor = context.actorOf(
          props = Props(new RequestResponseV1Router(config.requestResponseV1ActorsCount, requestResponseV1ActorProps, config.initializationTimeout)),
          name = classOf[RequestResponseV1Router].getSimpleName
        )

        context.watch(requestResponseV1Actor)

        Some(requestResponseV1Actor)
      }

      goto(ResourcesManager.Initializing) using new ResourcesManager.InitializingStateData(sd.resourcesInitializationTimeout, statisticalActor, requestResponseV1Actor.get)

    } catch {

      // cleanup of potentially allocated resources
      case e: Exception => {
        Try { statisticalActor.map(context.unwatch) }
        Try { statisticalActor.map(_ ! PoisonPill) }
        Try { requestResponseV1Actor.map(context.unwatch) }
        Try { requestResponseV1Actor.map(_ ! PoisonPill) }
        throw e
      }
    }
  }

  protected def requestResponseV1ActorInitializationComplete(sd: ResourcesManager.InitializingStateData): State = {

    val akkaHttpManager = context.actorOf(
      props = Props(new AkkaHttpManager(config.akkaHttpManager, sd.requestResponseV1Actor)),
      name = classOf[AkkaHttpManager].getSimpleName
    )

    context.watch(akkaHttpManager)

    stay using sd.copy(akkaHttpManager = Some(akkaHttpManager))
  }

  protected def akkaHttpManagerInitializationComplete(sd: ResourcesManager.InitializingStateData): State = {
    sd.resourcesInitializationTimeout.cancel
    context.parent ! ResourcesManager.InitializationComplete
    goto(ResourcesManager.Operating) using new ResourcesManager.OperatingStateData(sd.akkaHttpManager.get, sd.statisticalActor, sd.requestResponseV1Actor)
  }

  /**
   * Actor is finishing its work.
   */
  protected def terminate(reason: FSM.Reason, currentState: ResourcesManager.State, stateData: ResourcesManager.StateData): Unit = {

    // perform cleanup
    stateData match {

      case u: ResourcesManager.UninitializedStateData => {
        Try { u.resourcesInitializationTimeout.cancel }
      }

      case i: ResourcesManager.InitializingStateData => {
        Try { i.statisticalActor.map(context.unwatch) }

        Try { context.unwatch(i.requestResponseV1Actor) }
        Try { i.akkaHttpManager.map(context.unwatch) }

        Try { i.resourcesInitializationTimeout.cancel }
      }

      case o: ResourcesManager.OperatingStateData => {
        Try { o.statisticalActor.map(context.unwatch) }

        Try { context.unwatch(o.requestResponseV1Actor) }
        Try { context.unwatch(o.akkaHttpManager) }
      }
    }

    reason match {
      case FSM.Normal | FSM.Shutdown => log.warning(s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")

      case FSM.Failure(cause) => {
        val t = cause match {
          case t: Throwable => t
          case _            => new Exception(s"Failure cause ${cause.getClass.getName}: $cause.")
        }

        log.error(t, s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")
      }
    }
  }
}
