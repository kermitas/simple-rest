package as.simple.rest.actor.resources

import as.simple.rest.actor.http.AkkaHttpManagerConfig
import scala.concurrent.duration.FiniteDuration
import com.timgroup.statsd.StatsdConfig
import as.simple.rest.v1.actor.RequestResponseV1ActorConfig

/**
 * @param maxNrOfRetries number of retries (re-creation from the scratch) applied to child actor (if it thrown exception)
 *                       more here http://doc.akka.io/api/akka/2.3.10/?_ga=1.198366895.1217029815.1422907272#akka.actor.OneForOneStrategy
 * @param withinTimeRange if child actor thrown exception more than maxNrOfRetries times in this amount of time then this
 *                        actor will throw exception (that should be handled by its parent)
 *                        more here http://doc.akka.io/api/akka/2.3.10/?_ga=1.198366895.1217029815.1422907272#akka.actor.OneForOneStrategy
 * @param initializationTimeout after this time actor's initialization will be cancelled by throwing exception
 *                              (all already allocated resources will be disposed; parent actor - the supervisor - should take care about this exception)
 * @param printInternallyCollectedStatsTimeInterval how often (simple) internal stats collector should print report
 * @param requestResponseV1ActorsCount how many [[as.simple.rest.v1.actor.RequestResponseV1Actor]] actors should be created (they will be used concurrently by using strategy like round robin)
 */
case class ResourcesManagerConfig(
  maxNrOfRetries: Int,
  withinTimeRange: FiniteDuration,
  initializationTimeout: FiniteDuration,
  requestResponseV1Actor: RequestResponseV1ActorConfig,
  akkaHttpManager: AkkaHttpManagerConfig,
  printInternallyCollectedStatsTimeInterval: Option[FiniteDuration],
  requestResponseV1ActorsCount: Int
)