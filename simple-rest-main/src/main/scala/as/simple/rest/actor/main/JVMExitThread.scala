package as.simple.rest.actor.main

class JVMExitThread(delayInMs: Long, exitCode: Int, halt: Boolean = false) extends Thread {

  setDaemon(true)

  override def run: Unit = {
    Thread.sleep(delayInMs)

    if (halt)
      Runtime.getRuntime.halt(exitCode)
    else
      System.exit(exitCode)
  }
}