package as.simple.rest.actor.main

import as.simple.rest.actor.resources.ResourcesManagerConfig
import com.timgroup.statsd.StatsdConfig
import scala.concurrent.duration.FiniteDuration

/**
 * @param initializationTimeout after this time actor's initialization will be cancelled by throwing exception
 *                              (all already allocated resources will be disposed; parent actor - the supervisor - should take care about this exception)
 * @param actorSystemShutdownDelay after this time the actor system will be shutdown
 * @param installJvmShutdownSignalDetector install (or not) JVM shutdown hook
 * @param jvmShutdownDelayAfterJvmShutdownSignal keep JVM shutdown process for a give amount of time
 * @param maxNrOfRetries number of retries (re-creation from the scratch) applied to child actor (if it thrown exception)
 *                       more here http://doc.akka.io/api/akka/2.3.10/?_ga=1.198366895.1217029815.1422907272#akka.actor.OneForOneStrategy
 * @param withinTimeRange if child actor thrown exception more than maxNrOfRetries times in this amount of time then this
 *                        actor will throw exception (that should be handled by its parent)
 *                        more here http://doc.akka.io/api/akka/2.3.10/?_ga=1.198366895.1217029815.1422907272#akka.actor.OneForOneStrategy
 * @param installInputStreamDetector install actor that watches for data on process' input stream (if there are data then application shutdown will be initialized)
 * @param sendServiceIsUpStatsdSendInterval time interval to send 'I am alive' statsd
 * @param stats statsd details
 */
case class MainConfig(
  initializationTimeout: FiniteDuration,
  actorSystemShutdownDelay: FiniteDuration,
  installJvmShutdownSignalDetector: Boolean,
  jvmShutdownDelayAfterJvmShutdownSignal: FiniteDuration,
  maxNrOfRetries: Int,
  withinTimeRange: FiniteDuration,
  installInputStreamDetector: Boolean,
  resourcesManager: ResourcesManagerConfig,
  sendServiceIsUpStatsdSendInterval: FiniteDuration,
  stats: StatsdConfig
)