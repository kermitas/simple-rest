package as.simple.rest.actor.http

import akka.http.scaladsl.model.StatusCodes.Created
import akka.http.scaladsl.server.Directives._
import akka.stream.FlowMaterializer
import _root_.as.simple.rest.v1.RequestResponseV1
import _root_.as.simple.rest.v1.actor.ActorBasedRequestResponseV1
import _root_.as.simple.rest.v1.request.{ RequestV1, InvitationRequestV1, AddInviteeRequestV1, GetInvitationsRequestV1 }
import _root_.as.simple.rest.v1.response.{ AddInviteeResponseV1, GetInvitationsResponseV1 }
import spray.json.{ CompactPrinter, PrettyPrinter, JsonPrinter }
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration
import akka.actor.ActorRef
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import _root_.as.simple.rest.v1.{ AddInviteeRequest, AddInviteeResponse }
import _root_.as.simple.rest.v1.AddInviteeRequestSprayJsonProtocol.addInviteeRequestSprayJsonProtocol
import _root_.as.simple.rest.v1.AddInviteeResponseSprayJsonProtocol.addInviteeResponseSprayJsonProtocol
import _root_.as.simple.rest.v1.{ GetInvitationsRequest, GetInvitationsResponse }
import _root_.as.simple.rest.v1.GetInvitationsRequestSprayJsonProtocol.getInvitationsRequestSprayJsonProtocol
import _root_.as.simple.rest.v1.GetInvitationsResponseSprayJsonProtocol.getInvitationsResponseSprayJsonProtocol
import scala.util.{ Try, Success, Failure }

object RouteV1 {
  val someTrue = Some(true)
}

class RouteV1(
  requestResponseV1Actor: ActorRef,
  requestProcessingTimeout: FiniteDuration,
  prettyFormatResponseJson: Boolean,
  sendBackSuccessParameter: Boolean
)(implicit ec: ExecutionContext, materializer: FlowMaterializer) {

  import RouteV1._

  protected val requestResponseV1: RequestResponseV1 = new ActorBasedRequestResponseV1(requestResponseV1Actor, requestProcessingTimeout)
  protected implicit val jsonPrinter: JsonPrinter = if (prettyFormatResponseJson) PrettyPrinter else CompactPrinter

  /**
   * Incoming request routing definition.
   */
  val route: Route =
    (path(RequestV1.restVersion / InvitationRequestV1.restResource) & post) {

      entity(as[AddInviteeRequest]) { request =>

        val req = new AddInviteeRequestV1(
          invitee = request.invitee,
          timeoutInMs = request.timeoutInMs
        )

        /*
        // We don't return response JSON object in current scenario.

        val resp = requestResponseV1(req).mapTo[AddInviteeResponseV1].map { response =>

          val r = if (request.echoRequest.getOrElse(response.processingTime.isDefined)) Some(request) else None
          val pt = if (request.sendProcessingTime.getOrElse(response.processingTime.isDefined)) Some(response.processingTime.getOrElse(System.currentTimeMillis - req.processingStartTime)) else None
          val s = if (request.sendSuccess.getOrElse(sendBackSuccessParameter)) someTrue else None

          new AddInviteeResponse(
            request = r,
            processingTime = pt,
            success = s
          )
        }

        complete(resp)
        */

        // Let's just return 201 (Created) on success.
        val resp = requestResponseV1(req).mapTo[AddInviteeResponseV1].map(_.addingResult).map {
          _ match {

            case Success(addingResult) => if (addingResult)
              Created
            else
              throw new Exception("Could not add an invitation.")

            case Failure(t) => throw new Exception("Problem while adding invitation", t)
          }
        }

        complete(resp)
      }

    } ~
      (path(RequestV1.restVersion / InvitationRequestV1.restResource) & get) {

        // This is a GET request so user can not set optional parameters like echoRequest, sendProcessingTime and sendSuccess but
        // it is worth to remember that hey can be enabled in configuration (so there can be additional fields in response even if user didn't request them).

        val req = new GetInvitationsRequestV1(
          timeoutInMs = None
        )

        val resp = requestResponseV1(req).mapTo[GetInvitationsResponseV1].map { response =>

          val r = None
          val pt = if (response.processingTime.isDefined) Some(response.processingTime.getOrElse(System.currentTimeMillis - req.processingStartTime)) else None
          val s = if (sendBackSuccessParameter) someTrue else None

          new GetInvitationsResponse(
            invitations = response.invitations.get,
            request = r,
            processingTime = pt,
            success = s
          )
        }

        complete(resp)
      }
}