package as.simple.rest.actor.http

import akka.actor.{ ActorRef, Cancellable, OneForOneStrategy, SupervisorStrategy, Terminated }
import scala.util.{ Failure, Success }
import akka.http.scaladsl.Http
import scala.concurrent.Future

object AkkaHttpManager {
  sealed trait State
  case object Uninitialized extends State
  case object Initializing extends State
  case object Operating extends State

  sealed trait StateData
  case class UninitializedStateData(bindingTimeout: Cancellable) extends StateData
  case class InitializingStateData(binding: Future[Http.ServerBinding], bindingTimeout: Cancellable) extends StateData
  case class OperatingStateData(binding: Http.ServerBinding) extends StateData

  protected sealed trait Message extends Serializable
  protected sealed trait InternalMessage extends Message
  protected object BindingTimeout extends InternalMessage
  sealed trait OutgoingMessage extends Message

  /**
   * Send to parent actor when this actor is fully initialized and ready to work. Remember that actor can be restarted by supervisor and you can
   * receive this message more than once.
   */
  case object InitializationComplete extends OutgoingMessage
}

/**
 * This actor is responsible for binding to local port and setup Akka HTTP reactive stream (that serves responses on each REST request).
 */
class AkkaHttpManager(config: AkkaHttpManagerConfig, requestResponseV1Actor: ActorRef) extends AkkaHttpManagerImpl(config, requestResponseV1Actor) {

  import AkkaHttpManager._

  override val supervisorStrategy =
    OneForOneStrategy() {
      case e: Exception => SupervisorStrategy.Escalate
    }

  {
    val bindingTimeout = context.system.scheduler.scheduleOnce(config.bindingTimeout, self, BindingTimeout)(context.dispatcher)
    startWith(Uninitialized, new UninitializedStateData(bindingTimeout))
  }

  when(Uninitialized) {
    case Event(Uninitialized, sd: UninitializedStateData) => init(sd)
  }

  when(Initializing) {
    case Event(Success(binding: Http.ServerBinding), sd: InitializingStateData) => boundToLocalInterface(binding, sd)
    case Event(Failure(t), _) => throw new Exception(s"Could not bind to local interface.", t)
    case Event(BindingTimeout, _) => throw new Exception(s"Binding timeout (${config.bindingTimeout}).")
  }

  when(Operating) {
    case Event(true, _) => stay // nothing to do in this state
  }

  onTransition {
    case fromState -> toState => log.debug(s"changing state from ${fromState.getClass.getSimpleName} to ${toState.getClass.getSimpleName}")
  }

  whenUnhandled {
    case Event(Terminated(deadActor), _) => throw new Exception(s"""Watched actor "$deadActor" is dead, cannot continue.""")

    case Event(unhandledMessage, currentStateData) =>
      throw new Exception(s"Received unhandled message ${unhandledMessage.getClass.getName} ($unhandledMessage) from $sender in state $stateName (state data $currentStateData).")
  }

  onTermination {
    case StopEvent(reason, currentState, stateData) => terminate(reason, currentState, stateData)
  }

  initialize
  self ! Uninitialized
}
