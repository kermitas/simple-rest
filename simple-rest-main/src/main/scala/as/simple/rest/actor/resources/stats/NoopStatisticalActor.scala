package as.simple.rest.actor.resources.stats

import akka.actor.Actor

class NoopStatisticalActor extends Actor {

  override def receive = {
    case _ =>
  }
}
