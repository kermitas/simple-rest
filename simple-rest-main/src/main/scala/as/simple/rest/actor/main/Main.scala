package as.simple.rest.actor.main

import akka.JvmShutdownSignalDetector
import akka.actor.{ AllForOneStrategy, Cancellable, ActorRef, SupervisorStrategy, FSM, Terminated, KeyPressDetector }
import as.simple.rest.actor.resources.ResourcesManager
import com.timgroup.statsd.CloseableStatsDClient

object Main {
  sealed trait State
  case object Uninitialized extends State
  case object Initializing extends State
  case object Operating extends State

  sealed trait StateData
  case class UninitializedStateData(initializationTimeout: Cancellable) extends StateData
  case class InitializingStateData(initializationTimeout: Cancellable, jvmShutdownSignalDetector: JvmShutdownSignalDetector, keyPressDetector: ActorRef, resourcesManager: ActorRef, statsd: CloseableStatsDClient) extends StateData
  case class OperatingStateData(jvmShutdownSignalDetector: JvmShutdownSignalDetector, keyPressDetector: ActorRef, resourcesManager: ActorRef, statsd: CloseableStatsDClient, sendingServiceIsUp: Cancellable) extends StateData

  protected sealed trait Message extends Serializable
  protected sealed trait InternalMessage extends Message
  protected object InitializationTimeout extends InternalMessage
  sealed trait OutgoingMessage extends Message

  /**
   * Send to parent actor when this actor is fully initialized and ready to work. Remember that actor can be restarted by supervisor and you can
   * receive this message more than once.
   */
  case object InitializationComplete extends OutgoingMessage
}

/**
 * Main actor is created as first actor from JVM entry point.
 */
class Main(config: MainConfig) extends MainImpl(config) {

  import Main._

  override val supervisorStrategy =
    AllForOneStrategy(maxNrOfRetries = config.maxNrOfRetries, withinTimeRange = config.withinTimeRange) {
      case e: Exception => SupervisorStrategy.Restart
    }

  {
    val initializationTimeout = context.system.scheduler.scheduleOnce(config.initializationTimeout, self, InitializationTimeout)(context.dispatcher)
    startWith(Uninitialized, new UninitializedStateData(initializationTimeout))
  }

  when(Uninitialized) {
    case Event(Uninitialized, sd: UninitializedStateData) => init(sd)
  }

  when(Initializing) {
    case Event(ResourcesManager.InitializationComplete, sd: InitializingStateData) => resourceManagerInitializationComplete(sd)
    case Event(InitializationTimeout, _) => throw new Exception(s"Initialization timeout (${config.initializationTimeout}).")
  }

  when(Operating) {
    case Event(true, _) => stay // nothing to do in this state
  }

  onTransition {
    case fromState -> toState => log.debug(s"changing state from ${fromState.getClass.getSimpleName} to ${toState.getClass.getSimpleName}")
  }

  whenUnhandled {
    case Event(JvmShutdownSignalDetector.JvmShutdownSignalDetected, _) => {
      log.warning("JVM shutdown signal detected, stopping")
      stop(FSM.Normal)
    }

    case Event(KeyPressDetector.KeyWasPressed, _) => {
      log.warning("data on process' input stream detected (can be key press in console), stopping")
      stop(FSM.Normal)
    }

    case Event(initializationComplete @ ResourcesManager.InitializationComplete, _) => {
      // needed when child actor is restarted, for example: because of its exception (problem)
      log.warning(s"skipping ${initializationComplete.getClass.getName} message from $sender in state $stateName")
      stay
    }

    case Event(Terminated(deadActor), _) => throw new Exception(s"""Watched actor "$deadActor" is dead, cannot continue.""")

    case Event(unhandledMessage, currentStateData) =>
      throw new Exception(s"Received unhandled message ${unhandledMessage.getClass.getName} ($unhandledMessage) from $sender in state $stateName (state data $currentStateData).")
  }

  onTermination {
    case StopEvent(reason, currentState, stateData) => terminate(reason, currentState, stateData)
  }

  initialize
  self ! Uninitialized
}
