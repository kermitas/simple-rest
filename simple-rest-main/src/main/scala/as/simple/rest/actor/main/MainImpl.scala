package as.simple.rest.actor.main

import akka.JvmShutdownSignalDetector
import akka.actor.{ Cancellable, ActorRef, Props, KeyPressDetector, ActorSystem, FSM, PoisonPill }
import as.simple.rest.actor.resources.ResourcesManager
import com.timgroup.statsd.{ ConfigurableStatsDClient, CloseableStatsDClient }
import scala.util.Try
import scala.concurrent.duration.FiniteDuration
import akka.event.Logging

/**
 * Implementation for an actor.
 */
class MainImpl(config: MainConfig) extends FSM[Main.State, Main.StateData] {

  /**
   * Initialize and setup all needed resources and subsystems.
   */
  protected def init(sd: Main.UninitializedStateData): State = {
    var jvmShutdownSignalDetector: Option[JvmShutdownSignalDetector] = None
    var keyPressDetector: Option[ActorRef] = None
    var resourcesManager: Option[ActorRef] = None
    var statsd: Option[CloseableStatsDClient] = None

    try {
      jvmShutdownSignalDetector = if (config.installJvmShutdownSignalDetector) {
        Some(
          new JvmShutdownSignalDetector(
            jvmShutdownSignalListener = self,
            shutdownDelayInMs = config.jvmShutdownDelayAfterJvmShutdownSignal.toMillis,
            log = Some(Logging(context.system, classOf[JvmShutdownSignalDetector]))
          )
        )
      } else
        None

      jvmShutdownSignalDetector.map(Runtime.getRuntime.addShutdownHook)

      keyPressDetector = if (config.installInputStreamDetector) {
        Some(context.actorOf(
          props = Props(new KeyPressDetector(self)),
          name = classOf[KeyPressDetector].getSimpleName
        ))
      } else
        None

      keyPressDetector.map(context.watch)

      statsd = Some(new CloseableStatsDClient(ConfigurableStatsDClient(config.stats)))

      resourcesManager = Some(
        context.actorOf(
          props = Props(new ResourcesManager(config.resourcesManager, statsd.get)),
          name = classOf[ResourcesManager].getSimpleName
        )
      )

      resourcesManager.map(context.watch)

      goto(Main.Initializing) using new Main.InitializingStateData(
        sd.initializationTimeout,
        jvmShutdownSignalDetector = jvmShutdownSignalDetector.get,
        keyPressDetector = keyPressDetector.get,
        resourcesManager = resourcesManager.get,
        statsd = statsd.get
      )
    } catch {

      // cleanup of potentially allocated resources
      case e: Exception => {
        Try { jvmShutdownSignalDetector.map(Runtime.getRuntime.removeShutdownHook) }
        Try { resourcesManager.map(context.unwatch) }
        Try { resourcesManager.map(_ ! PoisonPill) }
        Try { keyPressDetector.map(context.unwatch) }
        Try { keyPressDetector.map(_ ! PoisonPill) }
        Try { statsd.map(_.close) }
        throw e
      }

    }
  }

  protected def resourceManagerInitializationComplete(sd: Main.InitializingStateData): State = {
    sd.initializationTimeout.cancel

    val sendingServiceIsUp = startSendingStatsThatThisInstanceIsUp(new MainStats(sd.statsd), config.sendServiceIsUpStatsdSendInterval)

    context.parent ! Main.InitializationComplete

    goto(Main.Operating) using new Main.OperatingStateData(
      jvmShutdownSignalDetector = sd.jvmShutdownSignalDetector,
      keyPressDetector = sd.keyPressDetector,
      resourcesManager = sd.resourcesManager,
      statsd = sd.statsd,
      sendingServiceIsUp = sendingServiceIsUp
    )
  }

  protected def startSendingStatsThatThisInstanceIsUp(stats: MainStats, sendServiceIsUpStatsdSendInterval: FiniteDuration): Cancellable =
    context.system.scheduler.schedule(sendServiceIsUpStatsdSendInterval, sendServiceIsUpStatsdSendInterval)(stats.sendServiceIsUp)(context.dispatcher)

  /**
   * Actor is finishing its work.
   */
  protected def terminate(reason: FSM.Reason, currentState: Main.State, stateData: Main.StateData): Unit = {

    def scheduleActorSystemShutdown(as: ActorSystem = context.system, actorSystemShutdownDelay: FiniteDuration = config.actorSystemShutdownDelay): Unit = {
      Try { log.warning(s"""shutting down actor system "${as.name}" in $actorSystemShutdownDelay""") }

      Try {
        as.scheduler.scheduleOnce(actorSystemShutdownDelay) {
          Try { log.warning(s"""shutting down actor system "${as.name}" now!""") }
          as.shutdown
        }(as.dispatcher)
      }
    }

    new JVMExitThread(config.actorSystemShutdownDelay.toMillis + 1000, 0, false).start
    new JVMExitThread(config.actorSystemShutdownDelay.toMillis + 1500, 0, true).start

    Try { scheduleActorSystemShutdown() }

    // perform cleanup
    stateData match {
      case u: Main.UninitializedStateData => {
        Try { u.initializationTimeout.cancel }
      }

      case i: Main.InitializingStateData => {
        Try { i.initializationTimeout.cancel }

        Try { Runtime.getRuntime.removeShutdownHook(i.jvmShutdownSignalDetector) }
        Try { context.unwatch(i.keyPressDetector) }
        Try { context.unwatch(i.resourcesManager) }
        Try { i.statsd.close }
      }

      case o: Main.OperatingStateData => {
        Try { Runtime.getRuntime.removeShutdownHook(o.jvmShutdownSignalDetector) }
        Try { context.unwatch(o.keyPressDetector) }
        Try { context.unwatch(o.resourcesManager) }
        Try { o.sendingServiceIsUp.cancel }
        Try { o.statsd.close }
      }
    }

    reason match {
      case FSM.Normal | FSM.Shutdown => log.warning(s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")

      case FSM.Failure(cause) => {
        val t = cause match {
          case t: Throwable => t
          case _            => new Exception(s"Failure cause ${cause.getClass.getName}: $cause.")
        }

        log.error(t, s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")
      }
    }

  }
}
