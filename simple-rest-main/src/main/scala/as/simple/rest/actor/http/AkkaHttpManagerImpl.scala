package as.simple.rest.actor.http

import akka.actor.{ FSM, ActorRef }
import akka.http.scaladsl.Http
import akka.stream.ActorFlowMaterializer
import akka.stream.scaladsl.{ Sink, Source }
import scala.concurrent.Future
import scala.util.Try

/**
 * Implementation for an actor.
 */
class AkkaHttpManagerImpl(config: AkkaHttpManagerConfig, requestResponseV1Actor: ActorRef) extends FSM[AkkaHttpManager.State, AkkaHttpManager.StateData] {

  import AkkaHttpManager._

  protected implicit val executionContext = context.dispatcher

  /**
   * Initialize binding to local interface.
   */
  protected def init(sd: UninitializedStateData): State = {
    var binding: Option[Future[Http.ServerBinding]] = None
    try {
      binding = Some(createAndStartAkkaReactiveStream)
      binding.get.onComplete(self ! _)
      goto(Initializing) using new InitializingStateData(binding.get, sd.bindingTimeout)
    } catch {

      // cleanup of potentially allocated resources
      case e: Exception => {
        Try { binding.map(_.map(_.unbind)) }
        throw e
      }

    }
  }

  /**
   * All needed resources are allocated, let's spawn child actor.
   */
  protected def boundToLocalInterface(binding: Http.ServerBinding, sd: InitializingStateData): State = {
    sd.bindingTimeout.cancel
    context.parent ! InitializationComplete
    goto(Operating) using OperatingStateData(binding)
  }

  protected def createAndStartAkkaReactiveStream: Future[Http.ServerBinding] = {

    implicit val materializer = ActorFlowMaterializer()(context)

    // binding 'stream'
    val serverSource: Source[Http.IncomingConnection, Future[Http.ServerBinding]] = Http(context.system).bind(
      interface = config.listenIp,
      port = config.listenPort
    )

    val routeV1 = new RouteV1(requestResponseV1Actor, config.requestProcessingTimeout, config.prettyFormatResponseJson, config.sendBackSuccessParameter)

    // TODO: exceptions (that can be thrown from Route) should be escalated via Main to root guard (that will restart Main actor
    // TODO: and thanks to that all resources will be released and allocated again)
    // TODO: that will clear/reset resources like Cassandra session (that - for example - we keep permanently opened and which can start failing because of, for example, network problem)
    serverSource.to(Sink.foreach(_.handleWith(routeV1.route))).run()
  }

  /**
   * Actor is finishing its work.
   */
  protected def terminate(reason: FSM.Reason, currentState: AkkaHttpManager.State, stateData: AkkaHttpManager.StateData): Unit = {

    // perform cleanup
    stateData match {
      case u: UninitializedStateData => {
        Try { u.bindingTimeout.cancel }
      }

      case i: InitializingStateData => {
        Try { i.bindingTimeout.cancel }
        Try { i.binding.map(_.unbind) }
      }

      case o: OperatingStateData => {
        Try { o.binding.unbind }
      }
    }

    reason match {
      case FSM.Normal | FSM.Shutdown => log.warning(s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")

      case FSM.Failure(cause) => {
        val t = cause match {
          case t: Throwable => t
          case _            => new Exception(s"Failure cause ${cause.getClass.getName}: $cause.")
        }

        log.error(t, s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")
      }
    }

  }
}
