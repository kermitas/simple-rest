package as.simple.rest.actor.http

import scala.concurrent.duration.FiniteDuration

/**
 * @param requestProcessingTimeout the general timeout of processing incoming request
 * @param listenIp local IP to bind to, here REST request should come
 * @param listenPort local port to bind to, here REST request should come
 * @param bindingTimeout after this time initialization will be cancelled (all already allocated resources will be disposed)
 *                       and this actor will die (which should be detected by the parent actor)
 * @param prettyFormatResponseJson how to format returned JSON
 * @param sendBackSuccessParameter determines if "success" parameter should be (or not) in response JSON
 */
case class AkkaHttpManagerConfig(
  listenIp: String,
  listenPort: Int,
  bindingTimeout: FiniteDuration,
  requestProcessingTimeout: FiniteDuration,
  prettyFormatResponseJson: Boolean,
  sendBackSuccessParameter: Boolean
)