package as.simple.rest.actor.main

import com.timgroup.statsd.StatsDClient
import scala.util.Random

object MainStats {
  val serviceIsUpStatsdKeyName = "service-is-up"
}

/**
 * Utility class that is a wrapper over statsd.
 */
class MainStats(statsDClient: StatsDClient) {

  import MainStats._

  protected val uniqueEventName = Random.nextLong.toString

  /**
   * Emit notification that this REST service is up and alive.
   */
  def sendServiceIsUp: Unit = statsDClient.set(serviceIsUpStatsdKeyName, uniqueEventName)
}
