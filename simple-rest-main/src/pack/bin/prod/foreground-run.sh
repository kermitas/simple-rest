#  === backup of log files and configuration ===

bin/prod/backup_current_data_prepare_new.sh start

#  === setting environmental variables

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

#  === run ===

export JVM_OPT="-Dconfig.file=data/current/config/application.conf -Dlogback.configurationFile=data/current/config/logback.xml"

bin/main