# ====== please don't execute this script directly ======

now=$(date -Iseconds)

# ====== create `data` folder ======

mkdir -v -p data
echo "[simple-rest] This folder should be a Docker volume (shared between host machine and Docker container)." > data/README.md

# ====== backup logs of previous instance ======

mkdir -v -p data/backup
echo "[simple-rest] This folder contains backup of data/current folder (backup is made on each start of the app)." > data/backup/README.md

mkdir -v -p data/backup/"$now"/previous_instance_logs
mv -v data/current/logs/*.* data/backup/"$now"/previous_instance_logs

# ====== copy default configuration from `config/default` to `data/current/config` (if there is none) ======

mkdir -v -p data/default
echo "[simple-rest] This folder contains application's default configuration (copied from Docker image of each start of the app) just to show you how defaults looks like. They are not used (so modifying them is useless)." > data/default/README.md

mkdir -v -p data/default/config
cp -v -f -r config/default/* data/default/config/

mkdir -v -p data/current
echo "[simple-rest] This folder contains data of currently living application (if there is any)." > data/current/README.md

mkdir -v -p data/current/config
echo "[simple-rest] This folder contains application's configuration. You can modify it and it will be used during next startup of the app. If not present then default one will be re-created from Docker image." > data/current/config/README.md
cp -v -n config/default/prod/*.* data/current/config/

# ====== backup configuration of currently starting instance ======

mkdir -v -p data/backup/"$now"/currently_starting_instance_config
cp -v data/current/config/*.* data/backup/"$now"/currently_starting_instance_config

# ====== prepare place for current logs ======

mkdir -v -p data/current/logs
echo "[simple-rest] This folder contains logs of currently living application (if there is any)." > data/current/logs/README.md