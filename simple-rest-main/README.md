#simple-rest-main
This is a runnable project.

## Usage
Please see **Grabbing all JARs, generating distribution, run** of `README.md` located in root folder.

Then you can try (port below depends on how you run the app; 8514 is for development, 9514 is for production): 

`curl -vvv --data '{"invitee":{"invitee":"John Smith","email":"john@smith.mx"}}' --header "Content-Type:application/json;charset=utf-8" http://localhost:8514/v1/invitation`

or

`curl -vvv http://localhost:8514/v1/invitation`

Currently we have:

- `/v1/invitation` - to store (POST) and retrieve (GET) invitation(s)

For more details please see API (`simple-rest-api` and `simple-rest-api-rest`).

## Run (distribution generated by `pack`)
Before run please copy `config/default/[prod|dev]/[application.conf and logback.xml]` file to `config/[prod|dev]/`. Then you can start just like that:

    ```
    [kermitas@server simple-rest]$ pwd
    /home/kermitas/simple-rest

    [kermitas@server simple-rest]$ export JVM_OPT="-Dconfig.file=/home/kermitas/simple-rest/config/default/prod/application.conf -Dlogback.configurationFile=/home/kermitas/simple-rest/config/default/prod/logback.xml"

    [kermitas@server simple-rest]$ bin/main          OR          [kermitas@server simple-rest]$ nohup bin/main 2>&1 &
    ```

## Run (using startup scripts)
The other way to start is use scripts located at `bin/[prod|dev]`.

Remember to make them executable:

    ```
    [kermitas@server simple-rest]$ pwd
    /home/kermitas/simple-rest
    
    [kermitas@server simple-rest]$ ls
    bin  config  data  lib
    
    [kermitas@server simple-rest]$ chmod -R 775 bin/
    ```

**Important note about startup scripts and configuration:**
*REMEMBER that you start with configuration that is in data/current/config. The configuration determinate that app will start as PROD or DEV (or custom). If configuration is not present it will be copied by the startup script from config/default ('prod' script will copy 'prod' configuration and 'dev' script will copy 'dev' configuration; that operation will be done only when there is no configuration at the data/current/config).*

So that is possible **that you will use 'dev' script but app will start as 'prod'** (because there were 'prod' config at data/current/config and - as it was there - it was not copied again by the startup script. Just be sure what configuration is at data/current/config and you can use any startup script.

## Logging (Logback)
Logging is done via *Logback*, you can provide your own configuration file `logback.xml` (for example via `java -Dlogback.configurationFile=/path/to/config.xml com.company.MyAppMain`). Please see default file `src/pack/config/default/logback.xml` (or `config/logback.xml` if you use generated distribution).

## Logging (Akka)
You can change log level by overriding `akka.loglevel = "DEBUG"` (allowed values are: `OFF`, `ERROR`, `WARNING`, `INFO`, `DEBUG`).

To achieve that in current project please see `application.conf`, find Akka configuration and key `loglevel`.

There are few ways how to override this property, please see [Typesafe Config](https://github.com/typesafehub/config) library.

## Docker
To prepare Docker image:

- copy (cloned from GIT) project on machine where `docker` command is available,
- **(only when using artifactory - like Artifactory - that has credentials)** remember to copy `.artifactoryDeployerCredentials` (the Artifactory credentials), put it in root folder (where `Dockerfile` is),
- **(only when using artifactory - like Artifactory - that has credentials)** also remember to publish to Artifactory (`+publish`) project `simple-rest-api` (SBT commands: `project simple-rest-api`, `+publish`),
- login to machine where you placed sources, change folder to this with project (where `Dockerfile` is),
- execute `docker build --tag="simple-rest-2015-06-15" .`.

To run execute `docker run -p 9514:9514 -v /home/kermitas/docker/simple-rest/data:/root/simple-rest/data -d simple-rest-2015-06-15` (where `/home/kermitas/docker/simple-rest/data` is location where you want to see simple-rest's files on host machine).