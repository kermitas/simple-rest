import sbt._
import Keys._

object SettingsScalaTest {
  def apply() = Seq(
    libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % Test
  )
}
