// This file contains a list of SBT plugins.

// ===============

resolvers += Resolver.sonatypeRepo("snapshots")
addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.7.0-SNAPSHOT")

// ===============

addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "4.0.0-RC2")

// ===============

resolvers += Classpaths.sbtPluginSnapshots
addSbtPlugin("com.danieltrinh" % "sbt-scalariform" % "1.3.0")

// ===============

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.7.5")

// ===============

resolvers += "Maven Central Repository" at "http://repo1.maven.org/maven2/"
addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.6.5")

// In case of SBT problems:
//   I am not sure but this line helps to avoid macro errors thrown by SBT while opening project.
//   In this line I include scala-reflect again because sbt-pack uses older Scala (that is how I understand this).
//
//  libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value

// ===============

addSbtPlugin("com.orrsella" % "sbt-stats" % "1.0.5")

// ===============

// let's ship `protoc` command internally so it will be no need to install it on developers machine
libraryDependencies ++= Seq("com.github.os72" % "protoc-jar" % "2.x.5")
// the http://trueaccord.github.io/ScalaPB SBT plugin
addSbtPlugin("com.trueaccord.scalapb" % "sbt-scalapb" % "0.4.14")

// ===============