import sbt._
import Keys._

/**
 * This file contains common settings.
 * Those settings needs to be implemented by all projects.
 */
object SettingsCommon {

  def apply(artifactConfig: ArtifactConfig, scalaVersions : SettingsScala.ScalaVersion*) =
    SettingsArtifact(artifactConfig) ++
    //SettingsArtifactory(SettingsArtifactory.DEPLOYING_AND_READING) ++ // in simple-rest project we didn't setup Artifactory yet
    SettingsScala(scalaVersions) ++
    SettingsScalaTest() ++
    SettingsDependencyGraph() ++
    SettingsScalariform() ++
    SettingsPack() ++
    otherSettings

  protected def otherSettings = Seq(
    // Cached resolution is an experimental feature of sbt added since 0.13.7 to address the scalability performance of
    // dependency resolution, http://www.scala-sbt.org/0.13/docs/Cached-Resolution.html
    updateOptions := updateOptions.value.withCachedResolution(true)
  )
}


