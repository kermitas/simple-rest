import sbt._
import Keys._

/**
 * Artifactory settings.
 *
 * Remember to put credentials to [home]/.ivy2/.artifactoryDeployerCredentials and [home]/.ivy2/.artifactoryReaderCredentials .
 */
object SettingsArtifactory {

  class ArtifactoryDetails(host: String, port: Int, repositoryName: String, serverName: String) {
    def url = s"http://$host:$port/artifactory/"
    def resolver = serverName at (url + repositoryName)
  }

  sealed abstract class RoleType(val credentials: Credentials) {
    def this(credentialsPath: File) = this(Credentials(credentialsPath))
    def this(credentialsFileName: String) = this(Path.userHome / ".ivy2" / credentialsFileName)
  }

  case object DEPLOYING_AND_READING extends RoleType(".artifactoryDeployerCredentials")
  case object READING_ONLY extends RoleType(".artifactoryReaderCredentials")

  lazy val releases = new ArtifactoryDetails(
    host = "localhost",
    port = 8181,
    repositoryName = "scala-releases",
    serverName = "Releases @ Artifactory")

  lazy val snapshots = new ArtifactoryDetails(
    host = "localhost",
    port = 8181,
    repositoryName = "scala-snapshots",
    serverName = "Snapshots @ Artifactory")

  /**
   * Returns settings for reading or deploying&reading.
   */
  def apply(settingType: RoleType) = {

    val creds = Seq(
      credentials += settingType.credentials
    )

    val readingSettings = Seq(
      resolvers += snapshots.resolver,
      resolvers += releases.resolver)

    val deploySettings = settingType match {

      case DEPLOYING_AND_READING => Seq(

        publishTo := Some(if(isSnapshot.value) snapshots.resolver else releases.resolver),

        // --- publishing details
        publishMavenStyle := true,
        publishArtifact in Test := false,
        pomIncludeRepository := { _ => false }
      )

      case READING_ONLY => Seq()
    }

    creds ++ readingSettings ++ deploySettings
  }
}
