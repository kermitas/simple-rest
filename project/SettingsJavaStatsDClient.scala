import sbt._
import Keys._

object SettingsJavaStatsDClient {
  def apply() = Seq(
    libraryDependencies += "com.timgroup" % "java-statsd-client" % "3.1.0"
  )
}
