import sbt._
import Keys._

object SettingsSpray {

  def sprayJson = Seq(
    libraryDependencies += "io.spray" %% "spray-json" % "1.3.1"
  )

}
