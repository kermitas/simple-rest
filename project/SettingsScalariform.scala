import com.typesafe.sbt.SbtScalariform._
import scalariform.formatter.preferences._

/**
 * Scalariform settings.
 */
object SettingsScalariform {

  def apply() = {
    scalariformSettings ++ {

      ScalariformKeys.preferences := FormattingPreferences()
        .setPreference(AlignSingleLineCaseStatements, true)
        .setPreference(MultilineScaladocCommentsStartOnFirstLine, false)
        .setPreference(PlaceScaladocAsterisksBeneathSecondAsterisk, false)

    }
  }
}

