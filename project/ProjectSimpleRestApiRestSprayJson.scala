import sbt._
import Keys._
import Implicits._
import SettingsScala.{ v210, v211 }
import com.trueaccord.scalapb.ScalaPbPlugin

object ProjectSimpleRestApiRestSprayJson {

  val subProjectName = "api-rest-spray-json"

  def apply(dependsOn: Project*)(implicit artifactConfig: ArtifactConfig) = settings(dependsOn: _*)(artifactConfig.copy(name = artifactConfig.name + "-" + subProjectName))

  protected def settings(dependsOn: Project*)(implicit artifactConfig: ArtifactConfig) =
    Project(
      id = artifactConfig.name,
      base = file(artifactConfig.name),

      aggregate = dependsOn,
      dependencies = dependsOn,
      delegates = dependsOn,

      settings = SettingsCommon(artifactConfig, v211, v210) ++
        ScalaPbPlugin.protobufSettings ++
        SettingsSpray.sprayJson
    )
}
