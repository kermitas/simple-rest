import sbt._
import Keys._

object SettingsScala {

  sealed class ScalaVersion(val version: String)
  object v210 extends ScalaVersion("2.10.5")
  object v211 extends ScalaVersion("2.11.6")

  def apply(sv: Seq[ScalaVersion]) = scalaByType(sv) ++ Seq(
    scalacOptions                   ++= Seq("-feature", "-unchecked", "-deprecation", "-Xfatal-warnings"),
    scalacOptions in (Compile, doc) ++= Seq("-diagrams", "-implicits") // generates diagrams in documentation, I am not sure if you should have
                                                                       // installed Graphviz http://www.graphviz.org/ (the "dot" command in your shell)
  )

  /**
   * First version will be used as current version.
   *
   * First and all others will be used as definition of cross-compilation.
   */
  protected def scalaByType(sv: Seq[ScalaVersion]) = Seq(
    scalaVersion := sv.head.version,
    crossScalaVersions := sv.map(_.version)
  )

  def scalaReflect() = Seq(
    libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value
  )
}
