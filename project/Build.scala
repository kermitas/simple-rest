object Build extends sbt.Build {

  implicit val artifactConfig = ArtifactConfig(
    name = "simple-rest",
    organization = "as",
    version = "0.1.0-SNAPSHOT"
  )

  // ==== projects definition

  // ---- utils projects (should be moved out to separate GIT repo and published in artifactory)

  lazy val akkaUtils = ProjectAkkaUtils()
  lazy val statsdUtils = ProjectTimgroupStatsdUtils()
  lazy val ficusUtils = ProjectFicusUtils()

  // ---- api projects

  // is used implicitly (via artifact) by ProjectSimpleRestApiRest
  // so remember to always publish (locally or to Artifactory) its latest version!!
  lazy val api = ProjectSimpleRestApi()

  lazy val apiRest = ProjectSimpleRestApiRest()
  lazy val apiActor = ProjectSimpleRestApiActor(api)

  lazy val provider1ApiActor = ProjectSimpleRestProvider1ApiActor(api)

  // ---- (de)serialization projects (spray-json)

  lazy val apiSprayJson = ProjectSimpleRestApiSprayJson(api)
  lazy val apiRestSprayJson = ProjectSimpleRestApiRestSprayJson(apiRest, apiSprayJson)

  // ---- implementation projects

  lazy val provider1InMemInvit = ProjectSimpleRestProvider1InMemInvit(provider1ApiActor, akkaUtils, statsdUtils)
  lazy val requestResponseV1 = ProjectSimpleRestRequestResponseV1(apiActor, provider1InMemInvit)

  // ---- executable projects

  lazy val main = ProjectSimpleRestMain(apiRestSprayJson, requestResponseV1, ficusUtils)
}
