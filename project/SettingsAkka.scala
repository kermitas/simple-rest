import sbt._
import Keys._

object SettingsAkka {

  val akkaVersion = "2.3.11"
  val akkaStreamsVersion = "1.0-RC3"

  def apply() = Seq(
    libraryDependencies += "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % akkaVersion
  )

  def streamsAndHttp() = Seq(
    libraryDependencies += "com.typesafe.akka" %% "akka-stream-experimental" % akkaStreamsVersion,
    libraryDependencies += "com.typesafe.akka" %% "akka-http-core-experimental" % akkaStreamsVersion,
    libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json-experimental" % akkaStreamsVersion
  )
}
