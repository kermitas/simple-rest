import sbt._
import Keys._
import Implicits._
import SettingsScala.{ v210, v211 }
import com.trueaccord.scalapb.ScalaPbPlugin

object ProjectSimpleRestApi {

  val subProjectName = "api"

  def apply(dependsOn: Project*)(implicit artifactConfig: ArtifactConfig) = settings(dependsOn: _*)(artifactConfig.copy(name = artifactConfig.name + "-" + subProjectName))

  protected def settings(dependsOn: Project*)(implicit artifactConfig: ArtifactConfig) =
    Project(
      id = artifactConfig.name,
      base = file(artifactConfig.name),

      aggregate = dependsOn,
      dependencies = dependsOn,
      delegates = dependsOn,

      settings = SettingsCommon(artifactConfig, v211, v210) ++
        ScalaPbPlugin.protobufSettings ++
        customScalaPBSettings
    )

  protected def customScalaPBSettings = Seq(
    ScalaPbPlugin.runProtoc in ScalaPbPlugin.protobufConfig := (args => com.github.os72.protocjar.Protoc.runProtoc("-v261" +: args.toArray)),
    ScalaPbPlugin.flatPackage in ScalaPbPlugin.protobufConfig := true, // do not use *.proto file names as packages structure
    unmanagedResourceDirectories in Compile <+= (sourceDirectory in ScalaPbPlugin.protobufConfig) // add *.proto files to generated JAR
  )
}
