import sbt._
import Keys._
import Implicits._
import SettingsScala.{ v210, v211 }
import com.trueaccord.scalapb.ScalaPbPlugin

object ProjectTimgroupStatsdUtils {

  val subProjectName = "timgroup-statsd-utils"

  def apply(dependsOn: Project*)(implicit artifactConfig: ArtifactConfig) = settings(dependsOn: _*)(artifactConfig.copy(name = subProjectName))

  protected def settings(dependsOn: Project*)(implicit artifactConfig: ArtifactConfig) =
    Project(
      id = artifactConfig.name,
      base = file(artifactConfig.name),

      aggregate = dependsOn,
      dependencies = dependsOn,
      delegates = dependsOn,

      settings = SettingsCommon(artifactConfig, v211, v210) ++
        SettingsScala.scalaReflect() ++
        SettingsJavaStatsDClient()
    )
}
