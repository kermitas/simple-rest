import sbt._
import Keys._
import Implicits._
import SettingsScala.{ v210, v211 }

object ProjectSimpleRestMain {

  val subProjectName = "main"
  val mainClassFullyQualifiedName: Option[String] = Some("as.simple.rest.Main")

  def apply(dependsOn: Project*)(implicit artifactConfig: ArtifactConfig) = settings(dependsOn: _*)(artifactConfig.copy(name = artifactConfig.name + "-" + subProjectName))

  protected def settings(dependsOn: Project*)(implicit artifactConfig: ArtifactConfig) =
    Project(
      id = artifactConfig.name,
      base = file(artifactConfig.name),

      aggregate = dependsOn,
      dependencies = dependsOn,
      delegates = dependsOn,

      settings = SettingsCommon(artifactConfig, v211, v210) ++
        mainClassSettings(mainClassFullyQualifiedName) ++
        SettingsAkka.streamsAndHttp() ++
        SettingsLogback()
    )

  protected def mainClassSettings(mainClassFullyQualifiedName: Option[String]) = Seq(
    mainClass in(Compile, run) := mainClassFullyQualifiedName
  )
}
