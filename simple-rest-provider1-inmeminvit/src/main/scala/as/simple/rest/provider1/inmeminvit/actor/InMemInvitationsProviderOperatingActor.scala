package as.simple.rest.provider1.inmeminvit.actor

import akka.actor.{ ActorLogging, OneForOneStrategy, SupervisorStrategy, ActorRef, Actor }
import as.simple.rest.provider1.inmeminvit.actor.utils.SendResponseUtils
import as.simple.rest.provider1.request.{ AddInvitee, GetInvitations }
import as.simple.rest.provider1.response.{ AddInviteeResult, GetInvitationsResult }
import scala.util.{ Try, Success }
import as.simple.rest.simplerest.Invitee
import scala.collection.mutable

object InMemInvitationsProviderOperatingActor {
  sealed trait Message extends Serializable
  sealed trait OutgoingMessage extends Message

  /**
   * Send to parent actor when this actor is fully initialized and ready to work. Remember that actor can be restarted by supervisor and you can
   * receive this message more than once.
   */
  case object InitializationComplete extends OutgoingMessage
}

/**
 * This actor is an extension of [[InMemInvitationsProviderActor.Operating]] state.
 */
class InMemInvitationsProviderOperatingActor(config: InMemInvitationsProviderActorConfig, responseUtils: SendResponseUtils) extends Actor with ActorLogging {

  import InMemInvitationsProviderOperatingActor._
  import context.dispatcher

  protected val invitations = mutable.ListBuffer[Invitee]()

  override val supervisorStrategy =
    OneForOneStrategy() {
      case e: Exception => SupervisorStrategy.Escalate
    }

  context.parent ! InitializationComplete

  override def receive = {
    case a: AddInvitee     => addInvitee(a, sender)
    case g: GetInvitations => getInvitations(g, sender)
  }

  protected def addInvitee(request: AddInvitee, requestSender: ActorRef): Unit = {

    // TODO: in current implementation we assume that adding new invitee takes no time

    val addResult = Try {
      invitations += request.invitee // TODO: danger, collection will grow without any limits; add limit to configuration and use it here
      log.debug(s"${classOf[AddInvitee].getSimpleName}: invitation added (currently there are ${invitations.size} invitations collected)")
      true
    }

    responseUtils.sendResponse(
      responseListener = requestSender,
      response = new AddInviteeResult(
      addingResult = addResult,
      request = request
    )
    )
  }

  protected def getInvitations(request: GetInvitations, requestSender: ActorRef): Unit = {

    // TODO: in current implementation we assume that taking whole collection of invitations from memory takes no time
    //
    // TODO: if obtaining right sequence of invitations can take more than zero time then we should spawn
    // TODO: here two concurrent Futures: one with 'real work' (obtaining invitations) and one with (Akka 'after' pattern) timeout (which should send Failure response AND
    // TODO: additionally revoke any side effects made during 'real work')

    log.debug(s"${classOf[GetInvitations].getSimpleName}: returning ${invitations.size} invitations")

    responseUtils.sendResponse(
      responseListener = requestSender,
      response = new GetInvitationsResult(
      invitations = Success(invitations),
      request = request
    )
    )
  }
}
