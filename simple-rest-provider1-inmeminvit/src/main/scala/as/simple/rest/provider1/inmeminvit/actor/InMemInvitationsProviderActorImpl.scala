package as.simple.rest.provider1.inmeminvit.actor

import akka.actor.{ ActorRef, FSM, Props, PoisonPill }
import as.simple.rest.provider1.inmeminvit.actor.utils.SendResponseUtils
import com.timgroup.statsd.{ ConfigurableStatsDClient, CloseableStatsDClient }
import scala.util.Try
import as.simple.rest.provider1.actor.RequestResponseActorApi
import as.simple.rest.provider1.request.Request

abstract class InMemInvitationsProviderActorImpl(config: InMemInvitationsProviderActorConfig) extends FSM[InMemInvitationsProviderActor.State, InMemInvitationsProviderActor.StateData] {

  private implicit val executionContext = context.dispatcher
  protected val statsd = new CloseableStatsDClient(ConfigurableStatsDClient(config.stats))
  protected val stats = new InMemInvitationsProviderActorStats(statsd, InMemInvitationsProviderActor.providerName)
  stats.sendNeutral
  protected val responseUtils = new SendResponseUtils(stats, log)

  /**
   * Start parallel initialization of all needed resources.
   */
  protected def init(sd: InMemInvitationsProviderActor.UninitializedStateData): State = {

    var operatingActor: Option[ActorRef] = None

    try {

      operatingActor = {
        val operatingActor = context.actorOf(
          props = Props(new InMemInvitationsProviderOperatingActor(config, responseUtils)),
          name = classOf[InMemInvitationsProviderOperatingActor].getSimpleName
        )

        context.watch(operatingActor)

        Some(operatingActor)
      }

    } catch {
      case e: Exception => {
        Try { operatingActor.map(context.unwatch) }
        Try { operatingActor.map(_ ! PoisonPill) }
        throw e
      }
    }

    goto(InMemInvitationsProviderActor.Initializing) using new InMemInvitationsProviderActor.InitializingStateData(sd.initializationTimeout, operatingActor.get)
  }

  protected def processRequestInOtherStateThanOperating(request: Request, requestSender: ActorRef): State = {
    val exception = new Exception(s"""Could not process request "$request" while ${getClass.getName} is not fully initialized.""")
    responseUtils.sendResponse(request, requestSender, exception)
    stay
  }

  protected def operatingActorIsInitialized(operatingActor: ActorRef, sd: InMemInvitationsProviderActor.InitializingStateData): State = {
    sd.initializationTimeout.cancel
    context.parent ! RequestResponseActorApi.InitializationComplete
    goto(InMemInvitationsProviderActor.Operating) using new InMemInvitationsProviderActor.OperatingStateData(operatingActor)
  }

  /**
   * Actor is finishing its work.
   */
  protected def terminate(reason: FSM.Reason, currentState: InMemInvitationsProviderActor.State, stateData: InMemInvitationsProviderActor.StateData): Unit = {

    // --- perform cleanup

    Try { statsd.close }

    stateData match {

      case u: InMemInvitationsProviderActor.UninitializedStateData => {
        Try { u.initializationTimeout.cancel }
      }

      case i: InMemInvitationsProviderActor.InitializingStateData => {
        Try { i.initializationTimeout.cancel }
        Try { context.unwatch(i.operatingActor) }
      }

      case o: InMemInvitationsProviderActor.OperatingStateData => {
        Try { context.unwatch(o.operatingActor) }
      }
    }

    // ---

    reason match {
      case FSM.Normal | FSM.Shutdown => log.warning(s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")

      case FSM.Failure(cause) => {
        val t = cause match {
          case t: Throwable => t
          case _            => new Exception(s"Failure cause ${cause.getClass.getName}: $cause.")
        }

        log.error(t, s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")
      }
    }
  }
}
