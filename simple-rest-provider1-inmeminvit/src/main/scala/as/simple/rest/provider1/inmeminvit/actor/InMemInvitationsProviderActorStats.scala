package as.simple.rest.provider1.inmeminvit.actor

import com.timgroup.statsd.StatsDClient

object InMemInvitationsProviderActorStats {
  val processedSuccessfullyStatsdKeyName = "processed-successfully"
  val processingFailureStatsdKeyName = "processing-failure"
  val processingTimeStatsdKeyName = "processing-time"
}

/**
 * Utility class that is a wrapper over statsd.
 */
class InMemInvitationsProviderActorStats(val statsDClient: StatsDClient, val providerName: String) {

  import InMemInvitationsProviderActorStats._

  protected val processedSuccessfullyStatsdProviderKeyName = processedSuccessfullyStatsdKeyName + "." + providerName
  protected val processingFailureStatsdProviderKeyName = processingFailureStatsdKeyName + "." + providerName
  protected val processingTimeStatsdProviderKeyName = processingTimeStatsdKeyName + "." + providerName

  /**
   * Emit notification about success.
   */
  def sendProcessingSuccessWithProcessingTime(processingTime: Long): Unit = {
    statsDClient.increment(processedSuccessfullyStatsdProviderKeyName)
    statsDClient.time(processingTimeStatsdProviderKeyName, processingTime)
  }

  /**
   * Emit notification about failure.
   */
  def sendProcessingFailure: Unit = statsDClient.increment(processingFailureStatsdProviderKeyName)

  /**
   * Sends "empty" statistics, not success and not failure.
   * Can be useful for initialization purposes (so Grafana will allow to create chart even if there were no events for many hours from first start).
   */
  def sendNeutral: Unit = {
    statsDClient.count(processedSuccessfullyStatsdProviderKeyName, 0)
    statsDClient.count(processingFailureStatsdProviderKeyName, 0)
    statsDClient.time(processingTimeStatsdProviderKeyName, 0)
  }
}
