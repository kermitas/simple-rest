package as.simple.rest.provider1.inmeminvit.actor

import com.timgroup.statsd.StatsdConfig
import scala.concurrent.duration.FiniteDuration

/**
 * @param stats statsd details
 * @param initializationTimeout after this time actor's initialization will be cancelled by throwing exception
 *                              (all already allocated resources will be disposed; parent actor - the supervisor - should take care about this exception)
 */
case class InMemInvitationsProviderActorConfig(
  stats: StatsdConfig,
  initializationTimeout: FiniteDuration
)
