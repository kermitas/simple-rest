package as.simple.rest.provider1.inmeminvit.actor

import akka.actor.{ SupervisorStrategy, OneForOneStrategy, Cancellable, ActorRef, Terminated }
import scala.concurrent.duration.FiniteDuration
import as.simple.rest.provider1.request.Request

object InMemInvitationsProviderActor {

  val providerName = "InMemoryInvitations"

  sealed trait State
  case object Uninitialized extends State
  case object Initializing extends State
  case object Operating extends State

  sealed trait StateData
  case class UninitializedStateData(initializationTimeout: Cancellable) extends StateData
  case class InitializingStateData(initializationTimeout: Cancellable, operatingActor: ActorRef) extends StateData
  case class OperatingStateData(operatingActor: ActorRef) extends StateData

  sealed trait Message extends Serializable
  sealed trait InternalMessage extends Message
  object InitializationTimeout extends InternalMessage
  case class RequestProcessingTimeout(timedOutRequest: Request) extends InternalMessage

  class RequestProcessingTimeoutException(val request: Request, val timeout: FiniteDuration) extends RuntimeException(s"""Request "$request" processing timeout ($timeout).""")
}

/**
 * This actor implements [[as.simple.rest.provider1.actor.RequestResponseActorApi]].
 */
class InMemInvitationsProviderActor(config: InMemInvitationsProviderActorConfig) extends InMemInvitationsProviderActorImpl(config) {

  import InMemInvitationsProviderActor._

  override val supervisorStrategy =
    OneForOneStrategy() {
      case e: Exception => SupervisorStrategy.Escalate
    }

  {
    val initializationTimeout = context.system.scheduler.scheduleOnce(config.initializationTimeout, self, InitializationTimeout)(context.dispatcher)
    startWith(Uninitialized, new UninitializedStateData(initializationTimeout))
  }

  when(Uninitialized) {
    case Event(Uninitialized, sd: UninitializedStateData) => init(sd)
  }

  when(Initializing) {
    case Event(InMemInvitationsProviderOperatingActor.InitializationComplete, sd: InitializingStateData) => operatingActorIsInitialized(sender, sd)
    case Event(InitializationTimeout, _) => throw new Exception(s"Resources initialization timeout (${config.initializationTimeout}).")
  }

  when(Operating) {
    case Event(request: Request, sd: OperatingStateData) => {
      sd.operatingActor.tell(request, sender)
      stay
    }
  }

  onTransition {
    case fromState -> toState => log.debug(s"changing state from ${fromState.getClass.getSimpleName} to ${toState.getClass.getSimpleName}")
  }

  whenUnhandled {
    case Event(Terminated(deadActor), _) => throw new Exception(s"""Watched actor "$deadActor" is dead, cannot continue.""")
    case Event(request: Request, _)      => processRequestInOtherStateThanOperating(request, sender)
    case Event(initializationComplete @ InMemInvitationsProviderOperatingActor.InitializationComplete, _) => {
      // needed when child actor is restarted, for example: because of its exception (problem)
      log.warning(s"skipping ${initializationComplete.getClass.getName} message from $sender in state $stateName")
      stay
    }
    case Event(unhandledMessage, currentStateData) =>
      throw new Exception(s"Received unhandled message ${unhandledMessage.getClass.getName} ($unhandledMessage) from $sender in state $stateName (state data $currentStateData).")
  }

  onTermination {
    case StopEvent(reason, currentState, stateData) => terminate(reason, currentState, stateData)
  }

  initialize
  self ! Uninitialized
}
