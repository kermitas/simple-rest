package as.simple.rest.provider1.inmeminvit.actor.utils

import akka.actor.ActorRef
import akka.event.LoggingAdapter
import as.simple.rest.provider1.inmeminvit.actor.InMemInvitationsProviderActorStats
import as.simple.rest.provider1.request.{ AddInvitee, GetInvitations, Request }
import as.simple.rest.provider1.response.{ AddInviteeResult, GetInvitationsResult, Response }
import scala.util.Failure

/**
 * Utility class for sending response.
 */
class SendResponseUtils(stats: InMemInvitationsProviderActorStats, log: LoggingAdapter)(implicit sender: ActorRef) {

  def sendResponse(response: Response, responseListener: ActorRef): Unit = {

    /**
     * Emitting stats.
     */
    def sendStats(response: Response): Unit = response match {
      case p: AddInviteeResult if (p.addingResult.isSuccess) => stats.sendProcessingSuccessWithProcessingTime(p.processingTime)
      case p: GetInvitationsResult if (p.invitations.isSuccess) => stats.sendProcessingSuccessWithProcessingTime(p.processingTime)
      case _ => stats.sendProcessingFailure
    }

    log.debug(s"""request "${response.request}" processing complete, processing time: ${response.processingTime}ms""")

    responseListener ! response
    sendStats(response)
  }

  def sendResponse(request: Request, responseListener: ActorRef, throwable: Throwable): Unit = request match {
    case g: AddInvitee => sendResponse(
      responseListener = responseListener,
      response = new AddInviteeResult(
      addingResult = Failure(throwable),
      request = g
    )
    )

    case g: GetInvitations => sendResponse(
      responseListener = responseListener,
      response = new GetInvitationsResult(
      invitations = Failure(throwable),
      request = g
    )
    )
  }
}
