package akka

import akka.actor.ActorRef
import akka.event.LoggingAdapter
import scala.util.Try

object JvmShutdownSignalDetector {
  val defaultShutdownDelayInMs = 3 * 1000

  sealed trait Message extends Serializable
  sealed trait OutgoingMessages extends Message
  object JvmShutdownSignalDetected extends OutgoingMessages
}

class JvmShutdownSignalDetector(
  jvmShutdownSignalListener: ActorRef,
  messageSendOnJVMShutdownSignal: Any = JvmShutdownSignalDetector.JvmShutdownSignalDetected,
  shutdownDelayInMs: Long = JvmShutdownSignalDetector.defaultShutdownDelayInMs,
  log: Option[LoggingAdapter] = None
) extends Thread {

  override def run: Unit = {
    Try { log.map(_.warning(s"""detected JVM shutdown request, sending notification ("${messageSendOnJVMShutdownSignal.getClass.getName}") to "$jvmShutdownSignalListener" and delay JVM shutdown by $shutdownDelayInMs ms""")) }
    jvmShutdownSignalListener ! messageSendOnJVMShutdownSignal
    Thread.sleep(shutdownDelayInMs)
    log.map(_.warning(s"""continuing JVM shutdown after $shutdownDelayInMs ms delay"""))
  }
}