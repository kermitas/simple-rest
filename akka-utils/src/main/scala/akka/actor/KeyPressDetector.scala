package akka.actor

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Try

object KeyPressDetector {

  lazy val checkConsoleInputInterval = 500 milliseconds

  sealed trait Message extends Serializable
  sealed trait OutgoingMessages extends Message
  object KeyWasPressed extends OutgoingMessages
}

class KeyPressDetector(
  keyPressListener: ActorRef,
  messageToSendOnDetectedKeyPress: Any = KeyPressDetector.KeyWasPressed,
  checkConsoleInputInterval: FiniteDuration = KeyPressDetector.checkConsoleInputInterval
) extends Actor with ActorLogging {

  protected val cancellable = context.system.scheduler.schedule(checkConsoleInputInterval, checkConsoleInputInterval, self, true)(context.dispatcher)

  override def postStop: Unit = {
    Try { cancellable.cancel }
    super.postStop
  }

  override def receive = {

    case true => if (Try { System.in.available > 0 }.getOrElse(false)) {
      Try { log.warning(s"""detected data on process' input stream (can be key press in console), sending notification ("${messageToSendOnDetectedKeyPress.getClass.getName}") to "$keyPressListener""""") }
      keyPressListener ! messageToSendOnDetectedKeyPress
    }

    case unhandledMessage => {
      val re = new RuntimeException(s"Received unhandled message $unhandledMessage.")
      log.error(re, "This is just an information exception, computation will be continued.")
    }
  }
}