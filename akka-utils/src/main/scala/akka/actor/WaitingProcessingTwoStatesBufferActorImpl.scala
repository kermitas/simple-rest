package akka.actor

import scala.reflect.runtime.universe._
import scala.util.Try
import scala.concurrent.duration.FiniteDuration
import scala.collection.mutable

/**
 * @param sendFailureResponseToCurrentAndAllQueuedRequestsOnActorFinish determines if failure ([[WaitingProcessingTwoStatesBufferActor.ActorFinishedException]]) response
 *                                                                 should be send to all queued requests and to currently processed
 *                                                                 when this actor will finish its work
 * @tparam RequestType the type that is recognized as request (that should be processed or queued)
 *                     in state [akka.actor.WaitingProcessingTwoStatesBufferActor.WaitingForRequest]] it will be executed immediately
 *                     but in [[akka.actor.WaitingProcessingTwoStatesBufferActor.ProcessingRequest]] it will be queued and executed in future
 * @tparam AttachmentType the type of attachment that is stored during execution of request
 */
abstract class WaitingProcessingTwoStatesBufferActorImpl[RequestType <: AnyRef: TypeTag, AttachmentType](sendFailureResponseToCurrentAndAllQueuedRequestsOnActorFinish: Boolean)
  extends FSM[WaitingProcessingTwoStatesBufferActor.State, WaitingProcessingTwoStatesBufferActor.StateData] {

  import WaitingProcessingTwoStatesBufferActor._

  /**
   * @param request real request that you would like to process
   * @return if you just skipped this message then stay(),
   *         else send something to other actor and goto(ProcessingRequestState) using new ProcessingRequestStateData(request, requestSender, requestProcessingTimeout)
   */
  protected def processIncomingRequestInWaitingState(request: RequestType, requestSender: ActorRef): State

  /**
   * @param message for example: timed out response (from some actor) on one of previous request (probably you would like to skip it)
   * @return if you just skipped this message then stay()
   */
  protected def processIncomingMessageInWaitingState(message: AnyRef, requestSender: ActorRef): State

  /**
   * Usually you want to queueRequest() (that will stay in this state()) or just reject this request (and also stay()).
   */
  protected def processIncomingRequestInProcessingState(request: RequestType, requestSender: ActorRef, sd: ProcessingRequestStateData[RequestType, AttachmentType]): State

  /**
   * @param message remember that it can be:
   *                1) response on currently processed request OR
   *                2) timed out response on previously processed request (that probably you would like to skip)
   * @return please finish with
   *         1) stay() if you just skipped incoming request
   *         2) process message and stay() or pickupNextFromQueueOrGoToWaitingForRequest() (if you are ready to process next queued message)
   */
  protected def processIncomingMessageInProcessingState(message: AnyRef, requestSender: ActorRef, sd: ProcessingRequestStateData[RequestType, AttachmentType]): State

  /**
   * Do something with timed out request.
   */
  protected def processTimedOutRequest(timedOutRequest: RequestType, requestSender: ActorRef, timeout: FiniteDuration, isCurrentRequest: Boolean): Unit

  /**
   * Please send this exception as an result of processing this request. Of course you can skip this and just do nothing.
   *
   * Usually this method is used when actor is terminating and wants to send failure responses to current and all queued requests.
   */
  protected def sendResponse(request: RequestType, responseListener: ActorRef, exception: Exception): Unit

  /**
   * We are in [[akka.actor.WaitingProcessingTwoStatesBufferActor.ProcessingRequest]] state and current request was processed so next one from the queue was picked up
   * and this method should do something with it.
   */
  protected def processQueuedRequestInProcessingState(request: RequestType, responseListener: ActorRef, requestProcessingTimeout: Option[Cancellable], sd: ProcessingRequestStateData[RequestType, AttachmentType]): AttachmentType

  protected def queueRequest(request: RequestType, requestSender: ActorRef, requestProcessingTimeout: Option[FiniteDuration], sd: ProcessingRequestStateData[RequestType, AttachmentType]): State = {
    val timeout = requestProcessingTimeout.map { requestProcessingTimeout =>
      context.system.scheduler.scheduleOnce(requestProcessingTimeout, self, new RequestProcessingTimeout(request, requestSender, requestProcessingTimeout))(context.dispatcher)
    }

    sd.requestsQueue += Tuple3(request, requestSender, timeout)
    stay
  }

  protected def requestProcessingTimeout(rpt: RequestProcessingTimeout[RequestType], sd: ProcessingRequestStateData[RequestType, AttachmentType]): State = {

    log.warning(s"""request "${rpt.timedOutRequest}" (that comes from ${rpt.requestSender}) processing timeout (${rpt.timeout})""")

    if (rpt.timedOutRequest.eq(sd.currentRequest)) {
      processTimedOutRequest(rpt.timedOutRequest, rpt.requestSender, rpt.timeout, true)
      pickupNextFromQueueOrGoToWaitingForRequest(sd)
    } else {
      processTimedOutRequest(rpt.timedOutRequest, rpt.requestSender, rpt.timeout, false)
      sd.requestsQueue.find(_._1.eq(rpt.timedOutRequest)).map(sd.requestsQueue -= _)
      stay
    }
  }

  protected def pickupNextFromQueueOrGoToWaitingForRequest(sd: ProcessingRequestStateData[RequestType, AttachmentType]): State =
    sd.requestsQueue.headOption match {
      case Some((request, responseListener, requestProcessingTimeout)) => {
        sd.requestsQueue.remove(0)
        val attachment = processQueuedRequestInProcessingState(request, responseListener, requestProcessingTimeout, sd)
        stay using new ProcessingRequestStateData[RequestType, AttachmentType](request, responseListener, requestProcessingTimeout, attachment, sd.requestsQueue)
      }

      case None => goto(WaitingForRequest) using WaitingForRequestStateData
    }

  protected def safeSendResponseToAll(requestWithResponseListenerAndTimeout: mutable.ListBuffer[(RequestType, ActorRef, Option[Cancellable])], exception: Exception): Unit =
    requestWithResponseListenerAndTimeout.foreach { t3 => Try { sendResponse(t3._1, t3._2, exception) } }

  /**
   * Actor is finishing its work.
   */
  protected def terminate(reason: FSM.Reason, currentState: WaitingProcessingTwoStatesBufferActor.State, stateData: WaitingProcessingTwoStatesBufferActor.StateData): Unit = {

    // --- perform cleanup

    stateData match {

      case p: ProcessingRequestStateData[RequestType, AttachmentType] => {
        Try { p.currentRequestTimeout.map(_.cancel) }
        Try { p.requestsQueue.map(_._3).map(c => Try { c.map(_.cancel) }) }

        Try {
          if (sendFailureResponseToCurrentAndAllQueuedRequestsOnActorFinish) {

            val exception = reason match {
              case FSM.Normal   => new ActorFinishedException(s"Could not process request because actor ${getClass.getSimpleName} finished its work.")
              case FSM.Shutdown => new ActorFinishedException(s"Could not process request because actor ${getClass.getSimpleName} was shutdown.")
              case FSM.Failure(cause) => cause match {
                case t: Throwable => {
                  val afe = new ActorFinishedException(s"Could not process request because actor ${getClass.getSimpleName} was terminated.")
                  afe.initCause(t)
                  afe
                }
                case _ => new ActorFinishedException(s"Could not process request because actor ${getClass.getSimpleName} was terminated.")
              }
            }

            Try { safeSendResponseToAll(p.requestsQueue, exception) }
            Try { sendResponse(p.currentRequest, p.currentRequestSender, exception) }
          }
        }
      }

      case _ =>
    }

    // ---

    reason match {
      case FSM.Normal | FSM.Shutdown => log.warning(s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")

      case FSM.Failure(cause) => {
        val t = cause match {
          case t: Throwable => t
          case _            => new Exception(s"Failure cause ${cause.getClass.getName}: $cause.")
        }

        log.error(t, s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")
      }
    }
  }
}
