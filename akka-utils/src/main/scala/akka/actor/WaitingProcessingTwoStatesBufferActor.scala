package akka.actor

import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration
import scala.reflect.runtime.universe._

object WaitingProcessingTwoStatesBufferActor {
  sealed trait State
  case object WaitingForRequest extends State
  case object ProcessingRequest extends State

  sealed trait StateData
  object WaitingForRequestStateData extends StateData
  class ProcessingRequestStateData[RequestType <: AnyRef: TypeTag, AttachmentType](
    val currentRequest: RequestType,
    val currentRequestSender: ActorRef,
    val currentRequestTimeout: Option[Cancellable],
    val attachment: AttachmentType,
    val requestsQueue: mutable.ListBuffer[(RequestType, ActorRef, Option[Cancellable])] = mutable.ListBuffer.empty[(RequestType, ActorRef, Option[Cancellable])]
  ) extends StateData

  sealed trait Message extends Serializable
  sealed trait InternalMessage extends Message
  class RequestProcessingTimeout[RequestType <: AnyRef: TypeTag](val timedOutRequest: RequestType, val requestSender: ActorRef, val timeout: FiniteDuration) extends InternalMessage

  class ActorFinishedException(message: String) extends RuntimeException(message)
}

/**
 * Two states actor that buffers incoming requests while it process first one.
 * When first one come then actor change state to [[akka.actor.WaitingProcessingTwoStatesBufferActor.ProcessingRequest]]
 * and all other requests should be queued (or simply rejected).
 *
 * All requests have timeout and can expire while being processed (or waiting on the queue).
 *
 * You must implement series of callback methods that are in [[WaitingProcessingTwoStatesBufferActorImpl]].
 *
 * @tparam RequestType the type that is recognized as request (that should be processed or queued)
 *                     in state [akka.actor.WaitingProcessingTwoStatesBufferActor.WaitingForRequest]] it will be executed immediately
 *                     but in [[akka.actor.WaitingProcessingTwoStatesBufferActor.ProcessingRequest]] it will be queued and executed in future
 * @tparam AttachmentType the type of attachment that is stored during execution of request
 */
abstract class WaitingProcessingTwoStatesBufferActor[RequestType <: AnyRef: TypeTag, AttachmentType](sendFailureResponseToCurrentAndAllQueuedRequestsOnActorFinish: Boolean)
  extends WaitingProcessingTwoStatesBufferActorImpl[RequestType, AttachmentType](sendFailureResponseToCurrentAndAllQueuedRequestsOnActorFinish) {

  import WaitingProcessingTwoStatesBufferActor._

  private val mirror = runtimeMirror(getClass.getClassLoader)

  startWith(WaitingForRequest, WaitingForRequestStateData)

  when(WaitingForRequest) {
    case Event(request: AnyRef, WaitingForRequestStateData) if mirror.runtimeClass(typeOf[RequestType]).isAssignableFrom(request.getClass) => processIncomingRequestInWaitingState(request.asInstanceOf[RequestType], sender)
    case Event(message: AnyRef, WaitingForRequestStateData) => processIncomingMessageInWaitingState(message, sender)
  }

  when(ProcessingRequest) {
    case Event(rpt: RequestProcessingTimeout[RequestType], sd: ProcessingRequestStateData[RequestType, AttachmentType]) => requestProcessingTimeout(rpt, sd)
    case Event(request: AnyRef, sd: ProcessingRequestStateData[RequestType, AttachmentType]) if mirror.runtimeClass(typeOf[RequestType]).isAssignableFrom(request.getClass) => processIncomingRequestInProcessingState(request.asInstanceOf[RequestType], sender, sd)
    case Event(message: AnyRef, sd: ProcessingRequestStateData[RequestType, AttachmentType]) => processIncomingMessageInProcessingState(message, sender, sd)
  }

  onTransition {
    case fromState -> toState => log.debug(s"changing state from ${fromState.getClass.getSimpleName} to ${toState.getClass.getSimpleName}")
  }

  whenUnhandled {
    case Event(unhandledMessage, currentStateData) =>
      throw new Exception(s"Received unhandled message ${unhandledMessage.getClass.getName} ($unhandledMessage) from $sender in state $stateName (state data $currentStateData).")
  }

  onTermination {
    case StopEvent(reason, currentState, stateData) => terminate(reason, currentState, stateData)
  }

  initialize
}
