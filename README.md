#simple-rest
This is a REST service that accumulates and serves invitations.

More in **README.md** of `simple-rest-main`.

## Usage
- use it (or one of sub-projects) as your dependency:

    ```scala
    libraryDependencies += "as" %% "simple-rest-api" % "xx.yy.zz-SNAPSHOT"  // instead of 'simple-rest-api' you can use directly one of sub-projects; instead of 'xx.yy.zz-SNAPSHOT' please use latest version from project/Build.scala
    ```

## SBT
#### Project structure
This is SBT multi-project. Command executed in one project will be forwarded down to its all sub-projects.

To refresh SBT configuration (after modifying configuration files in `project` subfolder) use `reload` command or restart SBT).

#### Compile
- clone into your local drive: `git clone https://bitbucket.org/kermitas/simple-rest.git`
- go to root folder (`cd simple-rest`)
- start SBT `sbt`
- to compile:
    - publish locally `simple-rest-api` (SBT commands: `project simple-rest-api`, `+publishLocal`)
    - goto whatever project you like, for example `project simple-rest-main`,
    - execute `compile` command
    - to work in continuous compilation use `~compile`
    - **the most preferred way is to always compile with tests** `~test:compile` 

#### Run
- you can run any executable sub-project (please find them in `project/Build.scala`) by executing `run` command

#### Cross compilation
- many commands (like `compile` or `publish-local`) prefixed with '+' will perform cross compilation

#### Unit tests
- `test` command will execute unit tests

#### IntelliJ Idea project
- if you are using [IntelliJ Idea](http://www.jetbrains.com/idea/) you can generate Idea project using `gen-idea` command

#### Eclipse project
- if you are using [Eclipse](https://eclipse.org/) you can generate Eclipse project using `eclipse` command (in Eclipse use the *Import Wizard* to import *Existing Projects into Workspace*)

#### Publish artifact
- publishing into your local repository `publish-local` (I recommend `+publish-local`)
- **(only when artifactory - like Artifactory - is used)** by using `publish` command (I recommend `+publish`) you can publish project to Artifactory

#### Grabbing all JARs, generating distribution, run
- you can use `pack` command to generate distribution with all JAR files
- generated distribution will be in `target/pack`
    - distributions will be generated for all sub-projects but only those with main class will be runnable
- if you want to run it:
    - first be sure that there is distribution folder `target/pack`, if there is no that means that this project can not be run (for example: it does not have main class defined), if yes change to this folder `cd target/pack`
    - run it be executing one of scripts in `bin` folder (for example: by executing `bin/run`)

#### Statistics
- `stats` command will print statistics (like number of Scala files, Java files, number of lines etc.)

#### Dependency graph
- project is using *sbt-dependency-graph* SBT plugin, execute `dependency-graph-ml` command to generate .graphml files in all *target* directories of all sub-projects (you can use [yEd](http://www.yworks.com/en/products_yfiles_about.html) to open them, then choose *Layout > Hierarchical*)

#### Code formatting
- code format is guarded by [Scalariform](https://github.com/daniel-trinh/scalariform), formatting is done before each compilation
- formatting rules are consistent for all projects of this SBT multi-project and are defined in `project/SettingsScalariform.scala`
- unformatted code is not allowed in this project

## GIT (source repository)
This repository works in branching model described in "[A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)".
