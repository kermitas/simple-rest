package as.simple.rest.v1.actor

import akka.actor.{ Terminated, ActorRef, Cancellable, OneForOneStrategy, SupervisorStrategy }
import com.timgroup.statsd.StatsDClient
import as.simple.rest.provider1.actor.{ RequestResponseActorApi => Provider1ActorApi }
import as.simple.rest.v1.request.RequestV1
import scala.concurrent.duration.FiniteDuration

object RequestResponseV1Actor {
  sealed trait State
  case object Uninitialized extends State
  case object Initializing extends State
  case object Operating extends State

  sealed trait StateData
  case class UninitializedStateData(initializationTimeout: Cancellable) extends StateData
  case class InitializingStateData(initializationTimeout: Cancellable, provider1: ActorRef, var alreadyInitialized: Int = 0, var operatingActor: Option[ActorRef] = None) extends StateData
  case class OperatingStateData(operatingActor: ActorRef, provider1: ActorRef) extends StateData

  sealed trait Message extends Serializable
  sealed trait InternalMessage extends Message
  object InitializationTimeout extends InternalMessage

  class RequestProcessingTimeoutException(val request: RequestV1, val timeout: FiniteDuration) extends RuntimeException(s"""Request "$request" processing timeout ($timeout).""")
}

/**
 * This actor implements [[as.simple.rest.v1.actor.RequestResponseV1ActorApi]].
 */
class RequestResponseV1Actor(
  config: RequestResponseV1ActorConfig,
  statisticalActor: ActorRef,
  statsd: StatsDClient
) extends RequestResponseV1ActorImpl(config, statisticalActor, statsd) {

  import RequestResponseV1Actor._

  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = config.maxNrOfRetries, withinTimeRange = config.withinTimeRange) {
      case e: Exception => SupervisorStrategy.Restart
    }

  {
    val resourcesInitializationTimeout = context.system.scheduler.scheduleOnce(config.initializationTimeout, self, InitializationTimeout)(context.dispatcher)
    startWith(Uninitialized, new UninitializedStateData(resourcesInitializationTimeout))
  }

  when(Uninitialized) {
    case Event(Uninitialized, sd: UninitializedStateData) => init(sd)
  }

  when(Initializing) {
    case Event(Provider1ActorApi.InitializationComplete, sd: InitializingStateData) => provider1InitializationConfirmation(sd)
    case Event(RequestResponseV1OperatingActor.InitializationComplete, sd: InitializingStateData) => operatingActorInitializationConfirmation(sender, sd)
    case Event(InitializationTimeout, _) => throw new Exception(s"Resources initialization timeout (${config.initializationTimeout}).")
  }

  when(Operating) {
    case Event(request: RequestV1, sd: OperatingStateData) => processRequest(request, sd, sender)
  }

  onTransition {
    case fromState -> toState => log.debug(s"changing state from ${fromState.getClass.getSimpleName} to ${toState.getClass.getSimpleName}")
  }

  whenUnhandled {
    case Event(Terminated(deadActor), _) => throw new Exception(s"""Watched actor "$deadActor" is dead, cannot continue.""")
    case Event(request: RequestV1, _)    => processRequestInOtherStateThanOperating(request, sender)
    case Event(initializationComplete @ Provider1ActorApi.InitializationComplete, _) => {
      // needed when child actor is restarted, for example: because of its exception (problem)
      log.warning(s"skipping ${initializationComplete.getClass.getName} message from $sender in state $stateName")
      stay
    }
    case Event(initializationComplete @ RequestResponseV1OperatingActor.InitializationComplete, _) => {
      // needed when child actor is restarted, for example: because of its exception (problem)
      log.warning(s"skipping ${initializationComplete.getClass.getName} message from $sender in state $stateName")
      stay
    }
    case Event(unhandledMessage, currentStateData) =>
      throw new Exception(s"Received unhandled message ${unhandledMessage.getClass.getName} ($unhandledMessage) from $sender in state $stateName (state data $currentStateData).")
  }

  onTermination {
    case StopEvent(reason, currentState, stateData) => terminate(reason, currentState, stateData)
  }

  initialize
  self ! Uninitialized
}