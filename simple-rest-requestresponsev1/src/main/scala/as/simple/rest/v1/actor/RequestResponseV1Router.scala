package as.simple.rest.v1.actor

import scala.concurrent.duration.FiniteDuration
import akka.actor.{ Terminated, Props, Actor, OneForOneStrategy, SupervisorStrategy, Cancellable }
import akka.routing.{ Router, ActorRefRoutee, RoundRobinRoutingLogic }

object RequestResponseV1Router {
  sealed trait Message extends Serializable
  sealed trait InternalMessage extends Message

  /**
   * Send to parent actor once initialization will be completed.
   */
  case object InitializationTimeout
}

/**
 * Router actor, the facade on [[RequestResponseV1Actor]].
 *
 * @param numberOfRoutees the number of routees (the [[RequestResponseV1Actor]]) to be created
 * @param initializationTimeout after this time actor's initialization will be cancelled by throwing exception
 *                              (all already allocated resources will be disposed; parent actor - the supervisor - should take care about this exception)
 */
class RequestResponseV1Router(numberOfRoutees: Int, requestResponseV1ActorProps: Props, initializationTimeout: FiniteDuration) extends Actor {

  import RequestResponseV1Router._

  override val supervisorStrategy =
    OneForOneStrategy() {
      case e: Exception => SupervisorStrategy.Escalate
    }

  protected val router = createRouter
  protected val initTimeout: Cancellable = context.system.scheduler.scheduleOnce(initializationTimeout, self, InitializationTimeout)(context.dispatcher)
  protected var initializedRoutees = 0

  /**
   * Create all routees.
   */
  protected def createRouter: Router = {
    var i = 0

    val routees = Vector.fill[ActorRefRoutee](numberOfRoutees) {

      val routee = context.actorOf(
        props = requestResponseV1ActorProps,
        name = classOf[RequestResponseV1Actor].getSimpleName + "-" + i
      )

      i += 1

      context.watch(routee)
      ActorRefRoutee(routee)
    }

    new Router(new RoundRobinRoutingLogic, routees)
  }

  /**
   * Initializing state that collect 'initialization complete' responses from all routees.
   */
  protected def initializingState: Receive = {

    // create all routees
    case RequestResponseV1ActorApi.InitializationComplete => {
      initializedRoutees += 1

      if (initializedRoutees >= numberOfRoutees) {
        initTimeout.cancel
        context.parent ! RequestResponseV1ActorApi.InitializationComplete
        context.become(operatingState)
      }
    }

    case InitializationTimeout => throw new Exception(s"Initialization timeout ($initializationTimeout).")

    case Terminated(deadActor) => throw new Exception(s"""Watched actor "$deadActor" is dead, cannot continue.""")
  }

  /**
   * Operating state.
   */
  protected def operatingState: Receive = {
    case Terminated(deadActor) => throw new Exception(s"""Watched actor "$deadActor" is dead, cannot continue.""")
    case messageToForward      => router.route(messageToForward, sender)
  }

  def receive = initializingState
}