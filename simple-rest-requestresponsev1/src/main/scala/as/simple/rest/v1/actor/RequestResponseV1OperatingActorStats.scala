package as.simple.rest.v1.actor

import com.timgroup.statsd.StatsDClient

object RequestResponseV1OperatingActorStats {
  val processedSuccessfullyStatsdKeyName = "processed-successfully"
  val processingFailureStatsdKeyName = "processing-failure"
  val processingTimeStatsdKeyName = "processing-time"
}

/**
 * Utility class that is a wrapper over statsd.
 */
class RequestResponseV1OperatingActorStats(val statsDClient: StatsDClient) {

  import RequestResponseV1OperatingActorStats._

  /**
   * Emit notification about success.
   */
  def sendProcessingSuccessWithProcessingTime(processingTime: Long): Unit = {
    statsDClient.increment(processedSuccessfullyStatsdKeyName)
    statsDClient.time(processingTimeStatsdKeyName, processingTime)
  }

  /**
   * Emit notification about failure.
   */
  def sendProcessingFailure = statsDClient.increment(processingFailureStatsdKeyName)

  /**
   * Sends "empty" statistics, not success and not failure.
   * Can be useful for initialization purposes (so Grafana will allow to create chart even if there were no events for many hours from first start).
   */
  def sendNeutral: Unit = {
    statsDClient.count(processedSuccessfullyStatsdKeyName, 0)
    statsDClient.count(processingFailureStatsdKeyName, 0)
    statsDClient.time(processingTimeStatsdKeyName, 0)

  }
}
