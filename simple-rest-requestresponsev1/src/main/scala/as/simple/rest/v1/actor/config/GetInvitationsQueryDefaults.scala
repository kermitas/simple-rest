package as.simple.rest.v1.actor.config

import scala.concurrent.duration.FiniteDuration

/**
 * @param timeout if timeout (the field located in API) is not present in request then this value will be used
 */
case class GetInvitationsQueryDefaults(
  timeout: FiniteDuration
)
