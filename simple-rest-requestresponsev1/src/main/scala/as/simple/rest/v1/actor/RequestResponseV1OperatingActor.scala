package as.simple.rest.v1.actor

import akka.actor.{ SupervisorStrategy, OneForOneStrategy, Cancellable, ActorRef, WaitingProcessingTwoStatesBufferActor }
import akka.pattern.ask
import as.simple.rest.provider1.request.{ AddInvitee, GetInvitations }
import as.simple.rest.provider1.response.{ Response => Provider1Response, AddInviteeResult, GetInvitationsResult }
import as.simple.rest.v1.request.{ RequestV1, AddInviteeRequestV1, GetInvitationsRequestV1 }
import as.simple.rest.v1.response.{ AddInviteeResponseV1, GetInvitationsResponseV1 }
import as.simple.rest.v1.actor.utils.SendResponseUtils
import scala.concurrent.duration.{ MILLISECONDS, FiniteDuration }

object RequestResponseV1OperatingActor {
  sealed trait Message extends Serializable
  sealed trait OutgoingMessage extends Message
  case object InitializationComplete extends OutgoingMessage
}

/**
 * This actor is an extension for [[RequestResponseV1Actor.Operating]] state.
 */
class RequestResponseV1OperatingActor(
  config: RequestResponseV1OperatingActorConfig,
  provider1: ActorRef,
  responseUtils: SendResponseUtils
) extends WaitingProcessingTwoStatesBufferActor[RequestV1, Any](true) {

  import WaitingProcessingTwoStatesBufferActor._

  override val supervisorStrategy =
    OneForOneStrategy() {
      case e: Exception => SupervisorStrategy.Escalate
    }

  context.parent ! RequestResponseV1OperatingActor.InitializationComplete

  /**
   * @return timeout extracted from request OR default one (taken from config)
   */
  protected def getRequestTimeout(request: RequestV1): FiniteDuration = request match {
    case a: AddInviteeRequestV1     => a.timeoutInMs.map(FiniteDuration(_, MILLISECONDS)).getOrElse(config.queryDefaults.addInvitee.timeout)
    case g: GetInvitationsRequestV1 => g.timeoutInMs.map(FiniteDuration(_, MILLISECONDS)).getOrElse(config.queryDefaults.getInvitations.timeout)
  }

  /**
   * @return simple response: can this (simple) request be send immediately to provider or no (because it is advanced message)
   */
  protected def canBeSendToProviderImmediately(request: RequestV1): Boolean = request match {
    case _: AddInviteeRequestV1     => true
    case _: GetInvitationsRequestV1 => true
  }

  /**
   * @return attachment and request processing timeout
   */
  protected def processRequest(request: RequestV1, requestSender: ActorRef): (Any, FiniteDuration) = {
    throw new Exception("None of request can be processed in this way.")
  }

  protected def processRequestThatCanBeSendImmediatelyToProvider(request: RequestV1, requestSender: ActorRef): Unit = request match {

    case a: AddInviteeRequestV1 => {
      val timeout = getRequestTimeout(a)

      val addInvitee = new AddInvitee(
        invitee = a.invitee,
        timeout = timeout
      )

      import context.dispatcher

      provider1.ask(addInvitee)(timeout).mapTo[AddInviteeResult].map(_.addingResult.get).onComplete { addingResult =>
        responseUtils.sendResponse(
          responseListener = requestSender,
          response = new AddInviteeResponseV1(
          addingResult = addingResult,
          request = Some(a),
          processingTime = Some(System.currentTimeMillis - a.processingStartTime)
        )
        )
      }
    }

    case g: GetInvitationsRequestV1 => {
      val timeout = getRequestTimeout(g)

      val getInvitations = new GetInvitations(
        timeout = timeout
      )

      import context.dispatcher

      provider1.ask(getInvitations)(timeout).mapTo[GetInvitationsResult].map(_.invitations.get).onComplete { invitations =>
        responseUtils.sendResponse(
          responseListener = requestSender,
          response = new GetInvitationsResponseV1(
          invitations = invitations,
          request = Some(g),
          processingTime = Some(System.currentTimeMillis - g.processingStartTime)
        )
        )
      }
    }
  }

  protected override def processIncomingRequestInWaitingState(request: RequestV1, requestSender: ActorRef): State = if (canBeSendToProviderImmediately(request)) {
    // this kind of 'simple' request can be automatically send to provider (and provider's response can be automatically mapped and send to back to requester
    processRequestThatCanBeSendImmediatelyToProvider(request, requestSender)
    stay
  } else {
    // this kind of 'advanced' request require from this actor to change state to ProcessingRequest, wait their while accumulating response (then send request to other provider,
    // again collect its result and finally send back response to requester)
    val requestSendToProviderAndItsTimeout = processRequest(request, requestSender)

    val requestProcessingTimeout = Some(
      context.system.scheduler.scheduleOnce(requestSendToProviderAndItsTimeout._2, self, new RequestProcessingTimeout[RequestV1](request, requestSender, requestSendToProviderAndItsTimeout._2))(context.dispatcher)
    )

    goto(ProcessingRequest) using new ProcessingRequestStateData[RequestV1, Any](request, requestSender, requestProcessingTimeout, requestSendToProviderAndItsTimeout._1)
  }

  protected override def processIncomingMessageInWaitingState(message: AnyRef, requestSender: ActorRef): State = message match {
    case provider1Response: Provider1Response => {
      log.warning(s"""Skipping not wanted (or timed out) response "$provider1Response" from provider1 in state "${WaitingForRequest.getClass.getSimpleName}".""")
      stay
    }
  }

  protected override def processIncomingRequestInProcessingState(request: RequestV1, requestSender: ActorRef, sd: ProcessingRequestStateData[RequestV1, Any]): State =
    if (canBeSendToProviderImmediately(request)) {
      // this kind of 'simple' request can be automatically send to provider (and provider's response can be automatically mapped and send to back to requester
      processRequestThatCanBeSendImmediatelyToProvider(request, requestSender)
      stay
    } else {
      // this kind of 'advanced' request needs to be queued and processed then its time will come
      queueRequest(request, requestSender, Some(getRequestTimeout(request)), sd)
    }

  protected override def processIncomingMessageInProcessingState(message: AnyRef, requestSender: ActorRef, sd: ProcessingRequestStateData[RequestV1, Any]): State = message match {
    case provider1Response: Provider1Response => {
      log.warning(s"""Skipping not wanted (or timed out) response "$provider1Response" from provider1 in state "${WaitingForRequest.getClass.getSimpleName}".""")
      stay
    }
  }

  protected override def processTimedOutRequest(timedOutRequest: RequestV1, requestSender: ActorRef, timeout: FiniteDuration, isCurrentRequest: Boolean): Unit = {
    val exception = new RequestResponseV1Actor.RequestProcessingTimeoutException(timedOutRequest, timeout)
    sendResponse(timedOutRequest, requestSender, exception)
  }

  protected override def sendResponse(request: RequestV1, responseListener: ActorRef, exception: Exception): Unit =
    responseUtils.sendResponse(request, responseListener, exception)

  protected override def processQueuedRequestInProcessingState(request: RequestV1, responseListener: ActorRef,
    requestProcessingTimeout: Option[Cancellable], sd: ProcessingRequestStateData[RequestV1, Any]): Any = processRequest(request, responseListener)._1

}

