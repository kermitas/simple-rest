package as.simple.rest.v1.actor

import akka.actor.{ ActorRef, FSM, Props, PoisonPill }
import as.simple.rest.provider1.inmeminvit.actor.InMemInvitationsProviderActor
import as.simple.rest.v1.request.RequestV1
import com.timgroup.statsd.StatsDClient
import scala.util.Try
import as.simple.rest.v1.actor.utils.SendResponseUtils

class RequestResponseV1ActorImpl(config: RequestResponseV1ActorConfig, statisticalActor: ActorRef, statsd: StatsDClient)
  extends FSM[RequestResponseV1Actor.State, RequestResponseV1Actor.StateData] {

  import RequestResponseV1Actor._

  protected val stats = new RequestResponseV1OperatingActorStats(statsd)
  stats.sendNeutral
  protected val responseUtils = new SendResponseUtils(config.operatingActor, statisticalActor, stats, log)

  /**
   * Start parallel initialization of all needed resources.
   */
  protected def init(sd: UninitializedStateData): State = {

    var provider1: Option[ActorRef] = None

    try {

      provider1 = {
        val provider1 = context.actorOf(
          props = Props(new InMemInvitationsProviderActor(config.inMemoryInvitations)),
          name = classOf[InMemInvitationsProviderActor].getSimpleName
        )

        context.watch(provider1)

        Some(provider1)
      }

      goto(Initializing) using new InitializingStateData(sd.initializationTimeout, provider1.get)

    } catch {

      // cleaning up potentially allocated resources
      case e: Exception => {
        Try { provider1.map(context.unwatch) }
        Try { provider1.map(_ ! PoisonPill) }
        throw e
      }
    }
  }

  /**
   * Spawn operating actor (all messages in [[Operating]] state will be forwarded to it).
   */
  protected def createOperatingActor(provider1: ActorRef): ActorRef = {
    val operatingActor = context.actorOf(
      props = Props(new RequestResponseV1OperatingActor(config.operatingActor, provider1, responseUtils)),
      name = classOf[RequestResponseV1OperatingActor].getSimpleName
    )

    context.watch(operatingActor)

    operatingActor
  }

  protected def provider1InitializationConfirmation(sd: InitializingStateData): State = {
    sd.alreadyInitialized += 1
    createOperatingActorIfAllNeededResourcesAreInitialized(sd)
  }

  protected def createOperatingActorIfAllNeededResourcesAreInitialized(sd: InitializingStateData): State = {
    if (sd.alreadyInitialized == 1) {
      sd.operatingActor = Some(createOperatingActor(sd.provider1))
    } else if (sd.alreadyInitialized > 1) {
      throw new Exception("This situation should not have place, there can be no more than 2 providers initialized.")
    }
    stay
  }

  protected def operatingActorInitializationConfirmation(operatingActor: ActorRef, sd: InitializingStateData): State = {
    sd.initializationTimeout.cancel
    context.parent ! RequestResponseV1ActorApi.InitializationComplete

    goto(Operating) using
      new OperatingStateData(operatingActor, sd.provider1)
  }

  /**
   * Just simple forward to operating actor.
   */
  protected def processRequest(request: RequestV1, sd: OperatingStateData, responseListener: ActorRef): State = {
    sd.operatingActor.tell(request, responseListener)
    stay
  }

  protected def processRequestInOtherStateThanOperating(request: RequestV1, requestSender: ActorRef): State = {
    val exception = new Exception(s"""Could not process request "$request" while ${getClass.getName} is not fully initialized.""")
    responseUtils.sendResponse(request, requestSender, exception)
    stay
  }

  /**
   * Actor is finishing its work.
   */
  protected def terminate(reason: FSM.Reason, currentState: RequestResponseV1Actor.State, stateData: RequestResponseV1Actor.StateData): Unit = {

    // perform cleanup
    stateData match {

      case u: UninitializedStateData => {
        Try { u.initializationTimeout.cancel }
      }

      case i: InitializingStateData => {
        Try { context.unwatch(i.provider1) }
        Try { i.operatingActor.map(context.unwatch) }
        Try { i.initializationTimeout.cancel }
      }

      case o: OperatingStateData => {
        Try { context.unwatch(o.provider1) }
        Try { context.unwatch(o.operatingActor) }
      }
    }

    reason match {
      case FSM.Normal | FSM.Shutdown => log.warning(s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")

      case FSM.Failure(cause) => {
        val t = cause match {
          case t: Throwable => t
          case _            => new Exception(s"Failure cause ${cause.getClass.getName}: $cause.")
        }

        log.error(t, s"stopped (${reason.getClass.getSimpleName}) in state $currentState (stateData $stateData)")
      }
    }
  }
}
