package as.simple.rest.v1.actor.utils

import akka.actor.ActorRef
import akka.event.LoggingAdapter
import as.simple.rest.v1.actor.{ RequestResponseV1OperatingActorStats, RequestResponseV1OperatingActorConfig }
import as.simple.rest.v1.request.{ RequestV1, AddInviteeRequestV1, GetInvitationsRequestV1 }
import as.simple.rest.v1.response.{ ResponseV1, AddInviteeResponseV1, GetInvitationsResponseV1 }
import scala.util.Failure

class SendResponseUtils(
  config: RequestResponseV1OperatingActorConfig,
  statisticalActor: ActorRef,
  stats: RequestResponseV1OperatingActorStats,
  log: LoggingAdapter
) {

  def sendResponse(response: ResponseV1, responseListener: ActorRef): Unit = {

    /**
     * Emitting stats.
     */
    def sendStats(processingTime: Option[Long]): Unit = processingTime match {
      case Some(processingTime) => {
        stats.sendProcessingSuccessWithProcessingTime(processingTime)
        statisticalActor ! "ok"
      }

      case None => {
        stats.sendProcessingFailure
        statisticalActor ! "err"
      }
    }

    log.debug(s"""request "${response.request}" processing complete, processing time: ${response.processingTime}ms""")

    // as a real response we send back less information if configuration says so
    val responseToSendWithOptionalProcessingTime: (ResponseV1, Option[Long]) = response match {

      case p: AddInviteeResponseV1 => (p.copy(
        request = if (config.echoRequest) p.request else None,
        processingTime = if (config.sendProcessingTime) p.processingTime else None
      ),
        if (p.addingResult.isSuccess) Some(p.processingTime.get) else None)

      case p: GetInvitationsResponseV1 => (p.copy(
        request = if (config.echoRequest) p.request else None,
        processingTime = if (config.sendProcessingTime) p.processingTime else None
      ),
        if (p.invitations.isSuccess) Some(p.processingTime.get) else None)
    }

    responseListener ! responseToSendWithOptionalProcessingTime._1

    sendStats(responseToSendWithOptionalProcessingTime._2)
  }

  def sendResponse(request: RequestV1, responseListener: ActorRef, throwable: Throwable): Unit = sendResponse(
    responseListener = responseListener,
    response = request match {

    case p: AddInviteeRequestV1 => {
      new AddInviteeResponseV1(
        addingResult = Failure(throwable),
        request = Some(p),
        processingTime = Some(System.currentTimeMillis - request.processingStartTime)
      )
    }

    case p: GetInvitationsRequestV1 => {
      new GetInvitationsResponseV1(
        invitations = Failure(throwable),
        request = Some(p),
        processingTime = Some(System.currentTimeMillis - request.processingStartTime)
      )
    }
  }
  )
}
