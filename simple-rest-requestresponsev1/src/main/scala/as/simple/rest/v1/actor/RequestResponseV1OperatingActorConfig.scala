package as.simple.rest.v1.actor

import as.simple.rest.v1.actor.config.QueryDefaultsConfig

/**
 * @param queryDefaults defaults for queries
 * @param echoRequest should request be echoed in returned JSON
 * @param sendProcessingTime should processing time be present in returned JSON
 */
case class RequestResponseV1OperatingActorConfig(
  queryDefaults: QueryDefaultsConfig,
  echoRequest: Boolean,
  sendProcessingTime: Boolean
)
