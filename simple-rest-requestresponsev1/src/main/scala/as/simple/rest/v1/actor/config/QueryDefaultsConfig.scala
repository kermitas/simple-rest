package as.simple.rest.v1.actor.config

/**
 * @param addInvitee defaults for this kind of query
 */
case class QueryDefaultsConfig(
  addInvitee: AddInviteeQueryDefaults,
  getInvitations: GetInvitationsQueryDefaults
)
