package as.simple.rest.v1.actor

import as.simple.rest.provider1.inmeminvit.actor.InMemInvitationsProviderActorConfig
import scala.concurrent.duration.FiniteDuration

/**
 * @param maxNrOfRetries number of retries (re-creation from the scratch) applied to child actor (if it thrown exception)
 *                       more here http://doc.akka.io/api/akka/2.3.10/?_ga=1.198366895.1217029815.1422907272#akka.actor.OneForOneStrategy
 * @param withinTimeRange if child actor thrown exception more than maxNrOfRetries times in this amount of time then this
 *                        actor will throw exception (that should be handled by its parent)
 * @param initializationTimeout after this time actor's initialization will be cancelled by throwing exception
 *                              (all already allocated resources will be disposed; parent actor - the supervisor - should take care about this exception)
 */
case class RequestResponseV1ActorConfig(
  maxNrOfRetries: Int,
  withinTimeRange: FiniteDuration,
  initializationTimeout: FiniteDuration,
  inMemoryInvitations: InMemInvitationsProviderActorConfig,
  operatingActor: RequestResponseV1OperatingActorConfig
)
