package as.simple.rest.v1

import spray.json._
import as.simple.rest.simplerest.Invitee
import as.simple.rest.simplerest.InviteeSprayJsonProtocol.inviteeSprayJsonProtocol

object AddInviteeRequestSprayJsonProtocol extends DefaultJsonProtocol {

  import RequestSprayJsonCommons._

  val invitee = "invitee"

  /**
   * JSON protocol for request.
   *
   * Done manually because automatic (de)serialization does not like those case classes.
   */
  implicit object addInviteeRequestSprayJsonProtocol extends RootJsonFormat[AddInviteeRequest] {
    override def write(p: AddInviteeRequest): JsValue = JsObject(
      Seq(
        Some(invitee -> p.invitee.toJson),

        p.timeoutInMs.map(timeoutInMs -> _.toJson),
        p.echoRequest.map(echoRequest -> _.toJson),
        p.sendProcessingTime.map(sendProcessingTime -> _.toJson),
        p.sendSuccess.map(sendSuccess -> _.toJson)
      ).filter(_.isDefined).map(_.get): _*
    )

    override def read(value: JsValue): AddInviteeRequest = {
      val valueAsJsObject = value.asJsObject

      value.asJsObject.getFields(invitee) match {
        case Seq(invitee) => new AddInviteeRequest(
          invitee = invitee.convertTo[Invitee],

          timeoutInMs = valueAsJsObject.getFields(timeoutInMs).headOption.map(_.asInstanceOf[JsNumber]).map(_.value.longValue),
          echoRequest = value.asJsObject.getFields(echoRequest).headOption.map(_.asInstanceOf[JsBoolean]).map(_.value),
          sendProcessingTime = value.asJsObject.getFields(sendProcessingTime).headOption.map(_.asInstanceOf[JsBoolean]).map(_.value),
          sendSuccess = value.asJsObject.getFields(sendSuccess).headOption.map(_.asInstanceOf[JsBoolean]).map(_.value)
        )
      }
    }
  }
}
