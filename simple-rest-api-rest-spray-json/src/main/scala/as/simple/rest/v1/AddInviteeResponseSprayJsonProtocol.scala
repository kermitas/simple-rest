package as.simple.rest.v1

import as.simple.rest.simplerest.Invitee
import as.simple.rest.simplerest.InviteeSprayJsonProtocol.inviteeSprayJsonProtocol
import AddInviteeRequestSprayJsonProtocol.addInviteeRequestSprayJsonProtocol
import spray.json._

object AddInviteeResponseSprayJsonProtocol extends DefaultJsonProtocol {

  import ResponseSprayJsonCommons._

  /**
   * JSON protocol for response.
   *
   * Done manually because automatic (de)serialization does not like those case classes.
   */
  implicit object addInviteeResponseSprayJsonProtocol extends RootJsonFormat[AddInviteeResponse] {
    override def write(p: AddInviteeResponse): JsValue = JsObject(
      Seq(
        p.request.map(request -> _.toJson),
        p.processingTime.map(processingTime -> _.toJson),
        p.success.map(success -> _.toJson)
      ).filter(_.isDefined).map(_.get): _*
    )

    override def read(value: JsValue): AddInviteeResponse = {
      val valueAsJsObject = value.asJsObject

      new AddInviteeResponse(
        request = valueAsJsObject.getFields(request).headOption.map(_.convertTo[AddInviteeRequest]),
        processingTime = valueAsJsObject.getFields(processingTime).headOption.map(_.asInstanceOf[JsNumber]).map(_.value.longValue),
        success = valueAsJsObject.getFields(success).headOption.map(_.asInstanceOf[JsBoolean]).map(_.value)
      )
    }
  }
}