package as.simple.rest.v1

import as.simple.rest.simplerest.Invitee
import as.simple.rest.simplerest.InviteeSprayJsonProtocol.inviteeSprayJsonProtocol
import as.simple.rest.v1.GetInvitationsRequestSprayJsonProtocol.getInvitationsRequestSprayJsonProtocol
import spray.json._

object GetInvitationsResponseSprayJsonProtocol extends DefaultJsonProtocol {

  import ResponseSprayJsonCommons._

  val invitations = "invitations"

  /**
   * JSON protocol for response.
   *
   * Done manually because automatic (de)serialization does not like those case classes.
   */
  implicit object getInvitationsResponseSprayJsonProtocol extends RootJsonFormat[GetInvitationsResponse] {
    override def write(p: GetInvitationsResponse): JsValue = JsObject(
      Seq(
        Some(invitations -> p.invitations.toJson),

        p.request.map(request -> _.toJson),
        p.processingTime.map(processingTime -> _.toJson),
        p.success.map(success -> _.toJson)
      ).filter(_.isDefined).map(_.get): _*
    )

    override def read(value: JsValue): GetInvitationsResponse = {
      val valueAsJsObject = value.asJsObject

      valueAsJsObject.getFields(invitations) match {
        case Seq(JsArray(invitations)) => new GetInvitationsResponse(
          invitations = invitations.map(_.convertTo[Invitee]),

          request = valueAsJsObject.getFields(request).headOption.map(_.convertTo[GetInvitationsRequest]),
          processingTime = valueAsJsObject.getFields(processingTime).headOption.map(_.asInstanceOf[JsNumber]).map(_.value.longValue),
          success = valueAsJsObject.getFields(success).headOption.map(_.asInstanceOf[JsBoolean]).map(_.value)
        )
      }
    }
  }
}