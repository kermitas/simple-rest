package as.simple.rest.v1

object RequestSprayJsonCommons {
  val timeoutInMs = "timeoutInMs"
  val echoRequest = "echoRequest"
  val sendProcessingTime = "sendProcessingTime"
  val sendSuccess = "sendSuccess"
}
