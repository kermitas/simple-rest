package as.simple.rest.v1

import spray.json._

object GetInvitationsRequestSprayJsonProtocol extends DefaultJsonProtocol {

  import RequestSprayJsonCommons._

  /**
   * JSON protocol for request.
   *
   * Done manually because automatic (de)serialization does not like those case classes.
   */
  implicit object getInvitationsRequestSprayJsonProtocol extends RootJsonFormat[GetInvitationsRequest] {
    override def write(p: GetInvitationsRequest): JsValue = JsObject(
      Seq(
        p.timeoutInMs.map(timeoutInMs -> _.toJson),
        p.echoRequest.map(echoRequest -> _.toJson),
        p.sendProcessingTime.map(sendProcessingTime -> _.toJson),
        p.sendSuccess.map(sendSuccess -> _.toJson)
      ).filter(_.isDefined).map(_.get): _*
    )

    override def read(value: JsValue): GetInvitationsRequest = {
      val valueAsJsObject = value.asJsObject

      new GetInvitationsRequest(
        timeoutInMs = valueAsJsObject.getFields(timeoutInMs).headOption.map(_.asInstanceOf[JsNumber]).map(_.value.longValue),
        echoRequest = value.asJsObject.getFields(echoRequest).headOption.map(_.asInstanceOf[JsBoolean]).map(_.value),
        sendProcessingTime = value.asJsObject.getFields(sendProcessingTime).headOption.map(_.asInstanceOf[JsBoolean]).map(_.value),
        sendSuccess = value.asJsObject.getFields(sendSuccess).headOption.map(_.asInstanceOf[JsBoolean]).map(_.value)
      )
    }
  }
}
