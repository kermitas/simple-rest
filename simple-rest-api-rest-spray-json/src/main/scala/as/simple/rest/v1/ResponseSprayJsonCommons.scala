package as.simple.rest.v1

object ResponseSprayJsonCommons {
  val request = "request"
  val processingTime = "processingTime"
  val success = "success"
}
