package com.timgroup.statsd

import java.io.Closeable

class CloseableStatsDClient(c: StatsDClient) extends StatsDClient with Closeable {

  override def close: Unit = stop

  override def recordSetEvent(aspect: String, eventName: String): Unit = c.recordSetEvent(aspect, eventName)

  override def count(aspect: String, delta: Long): Unit = c.count(aspect, delta)

  override def count(aspect: String, delta: Long, sampleRate: Double): Unit = c.count(aspect, delta, sampleRate)

  override def decrementCounter(aspect: String): Unit = c.decrementCounter(aspect)

  override def set(aspect: String, eventName: String): Unit = c.set(aspect, eventName)

  override def recordExecutionTimeToNow(aspect: String, systemTimeMillisAtStart: Long): Unit = c.recordExecutionTimeToNow(aspect, systemTimeMillisAtStart)

  override def stop(): Unit = c.stop

  override def increment(aspect: String): Unit = c.increment(aspect)

  override def decrement(aspect: String): Unit = c.decrement(aspect)

  override def incrementCounter(aspect: String): Unit = c.incrementCounter(aspect)

  override def gauge(aspect: String, value: Long): Unit = c.gauge(aspect, value)

  override def gauge(aspect: String, value: Double): Unit = c.gauge(aspect, value)

  override def time(aspect: String, value: Long): Unit = c.time(aspect, value)

  override def recordGaugeDelta(aspect: String, delta: Long): Unit = c.recordGaugeDelta(aspect, delta)

  override def recordGaugeDelta(aspect: String, delta: Double): Unit = c.recordGaugeDelta(aspect, delta)

  override def recordGaugeValue(aspect: String, value: Long): Unit = c.recordGaugeValue(aspect, value)

  override def recordGaugeValue(aspect: String, value: Double): Unit = c.recordGaugeValue(aspect, value)

  override def recordExecutionTime(aspect: String, timeInMs: Long): Unit = c.recordExecutionTime(aspect, timeInMs)

  override def recordExecutionTime(aspect: String, timeInMs: Long, sampleRate: Double): Unit = c.recordExecutionTime(aspect, timeInMs, sampleRate)
}