package com.timgroup.statsd

object ConfigurableStatsDClient {
  def apply(config: StatsdConfig): StatsDClient = if (config.enabled) {
    new NonBlockingStatsDClient(config.prefix, config.host, config.port)
  } else {
    new NoOpStatsDClient
  }
}
