package com.timgroup.statsd

case class StatsdConfig(
  enabled: Boolean,
  host: String,
  port: Int,
  prefix: String
)
