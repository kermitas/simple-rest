package as.simple.rest.provider1

import as.simple.rest.provider1.request.Request
import as.simple.rest.provider1.response.Response
import scala.concurrent.Future

/**
 * This is the main interface of this "provider".
 */
trait RequestResponse {

  /**
   * request -> to -> response transformer
   */
  def apply(request: Request): Future[Response]

}