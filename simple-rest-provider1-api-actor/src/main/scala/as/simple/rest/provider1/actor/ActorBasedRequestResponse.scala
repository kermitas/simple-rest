package as.simple.rest.provider1.actor

import akka.actor.ActorRef
import akka.util.Timeout
import akka.pattern.ask
import as.simple.rest.provider1.RequestResponse
import as.simple.rest.provider1.request.Request
import as.simple.rest.provider1.response.Response
import scala.concurrent.{ Future, ExecutionContext }

/**
 * The implementation of [[as.simple.rest.provider1.RequestResponse]] that will use actor under the hood.
 *
 * @param providerActor the actor that implements [[as.simple.rest.provider1.actor.RequestResponseActorApi]]
 * @param responseTimeout timeout while waiting for response
 */
class ActorBasedRequestResponse(val providerName: String, providerActor: ActorRef, responseTimeout: Timeout)(implicit ec: ExecutionContext) extends RequestResponse {

  override def apply(request: Request): Future[Response] = providerActor.ask(request)(responseTimeout).mapTo[Response]

}