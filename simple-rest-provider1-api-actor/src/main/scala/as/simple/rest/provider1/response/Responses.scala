package as.simple.rest.provider1.response

import as.simple.rest.provider1.request.{ Request, AddInvitee, GetInvitations }
import as.simple.rest.simplerest.Invitee
import scala.util.Try

sealed trait Response extends Serializable {
  val request: Request
  val processingTime: Long
}

class AddInviteeResult(
  val addingResult: Try[Boolean],
  val request: AddInvitee
) extends Response {

  val processingTime: Long = System.currentTimeMillis - request.processingStartTime

}

class GetInvitationsResult(
  val invitations: Try[Seq[Invitee]],
  val request: GetInvitations
) extends Response {

  val processingTime: Long = System.currentTimeMillis - request.processingStartTime

}