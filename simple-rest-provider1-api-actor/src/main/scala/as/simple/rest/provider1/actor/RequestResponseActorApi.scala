package as.simple.rest.provider1.actor

import as.simple.rest.provider1.request.Request
import as.simple.rest.provider1.response.Response

/**
 * Actor's (message) API.
 *
 * When communicating with actor that implements this API just send [[Request]] to it and get [[Response]] as a response (or nothing so remember to set timeout).
 */
object RequestResponseActorApi {
  sealed trait Message extends Serializable
  sealed trait OutgoingMessage extends Message

  /**
   * Send to parent actor when this actor is fully initialized and ready to work. Remember that actor can be restarted by supervisor and you can
   * receive this message more than once.
   */
  case object InitializationComplete extends OutgoingMessage

}