package as.simple.rest.provider1.request

import as.simple.rest.simplerest.Invitee
import scala.concurrent.duration.FiniteDuration

sealed trait Request extends Serializable {
  val timeout: FiniteDuration
  val processingStartTime: Long
}

class AddInvitee(
  val invitee: Invitee,
  val timeout: FiniteDuration,
  val processingStartTime: Long = System.currentTimeMillis
) extends Request

class GetInvitations(
  val timeout: FiniteDuration,
  val processingStartTime: Long = System.currentTimeMillis
) extends Request