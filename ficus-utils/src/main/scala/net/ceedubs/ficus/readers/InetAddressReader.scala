package net.ceedubs.ficus.readers

import java.net.InetAddress
import com.typesafe.config.Config

/**
 * This is a helper class for Ficus for reading java.net.InetAddress.
 */
object InetAddressReader {

  implicit val inetAddressReader: ValueReader[InetAddress] = new ValueReader[InetAddress] {
    override def read(config: Config, path: String) = InetAddress.getByName(config.getString(path))
  }

}
