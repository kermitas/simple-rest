package as.simple.rest.simplerest

import spray.json._

object InviteeSprayJsonProtocol extends DefaultJsonProtocol {

  val invitee = "invitee"
  val email = "email"

  /**
   * JSON protocol for response.
   *
   * Done manually because automatic (de)serialization does not like those case classes.
   */
  implicit object inviteeSprayJsonProtocol extends RootJsonFormat[Invitee] {
    override def write(p: Invitee): JsValue = JsObject(
      invitee -> p.invitee.toJson,
      email -> p.email.toJson
    )

    override def read(value: JsValue): Invitee = value.asJsObject.getFields(invitee, email) match {
      case Seq(JsString(invitee), JsString(email)) => new Invitee(
        invitee = invitee,
        email = email
      )
    }
  }
}