FROM centos

MAINTAINER Artur Stanek <kermitas@gmail.com>

# ===================================

RUN yum -y update && \
    yum -y install wget unzip

# downloading & installing SBT, removing ZIP
RUN cd /root && \
    wget http://dl.bintray.com/sbt/native-packages/sbt/0.13.8/sbt-0.13.8.zip && \
    unzip sbt-0.13.8.zip && \
    rm -v /root/sbt-0.13.8.zip

# downloading and installing Oracle Java (JDK), removing RPM
RUN cd /root && \
    wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u45-b14/jdk-8u45-linux-x64.rpm" && \
    yum -y install jdk-8u45-linux-x64.rpm && \
    rm -v jdk-8u45-linux-x64.rpm

# initializing SBT (first run, it will download all SBT needs including SBT itself)
RUN cd /root && \
    /root/sbt/bin/sbt exit

# copying content of current folder (located at host) to guest; remember to put Artifactory's credentials (the .artifactoryDeployerCredentials file) if Artifactory is used
RUN mkdir -v -p /root/simple-rest/
COPY . /root/simple-rest/
# RUN mv -v /root/simple-rest/.artifactoryDeployerCredentials /root/.ivy2

# SBT will download all needed dependencies
RUN cd /root/simple-rest && \
    /root/sbt/bin/sbt update

# SBT will compile project and prepare distribution
RUN cd /root/simple-rest && \
    /root/sbt/bin/sbt pack

# remove sources, put prepared distribution in /root/simple-rest, make files in bin folder executable
RUN mkdir -v -p /root/compiled-simple-rest/ && \
    mv -v /root/simple-rest/simple-rest-main/target/pack/* /root/compiled-simple-rest/ && \
    rm -v -r /root/simple-rest && \
    mkdir -v -p /root/simple-rest && \
    mv -v /root/compiled-simple-rest/* /root/simple-rest/ && \
    rm -v -r /root/compiled-simple-rest && \
    chmod -v -R 775 /root/simple-rest/bin

# remove SBT and ivy2 repo
RUN rm -v -r /root/sbt && \
    rm -v -r /root/.sbt && \
    rm -v -r /root/.ivy2

# ===================================

# this is a port number, you can re-map it to any port you want on host machine
EXPOSE 9514

# here is a shared folder, you can link it to some location on host machine
VOLUME ["/root/simple-rest/data"]

# this is "main command" that will be executed after: docker run -p 9514:9514 -v /home/kermitas/docker/simple-rest/data:/root/simple-rest/data -d name-of-the-docker-image-goes-here
ENTRYPOINT cd /root/simple-rest && bin/prod/foreground-run.sh

# ===================================